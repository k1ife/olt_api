<?php

namespace common\models\utils;


/**
 * Class TreeItem
 * @property $id
 * @property $data
 * @property TreeItem[] $children
 * @property TreeItem $parent
 * @property $filter
 */
class TreeItem
{
    public $id;
    public $data;
    public $children;
    protected $parent;
    protected $filter;

    function __construct($id, $data, $filter, TreeItem $parent = null)
    {
        $this->id = $id;
        $this->data = $data;
        $this->filter = $filter;
        $this->children = [];
        if ($parent){
            $parent->addChild($this);
        }
    }

    public function addChild(TreeItem $item)
    {
        $this->children[$item->id] = $item;
        $item->parent = $this;
    }

    public function deleteChild_internal($item_id)
    {
        if (isset($this->children[$item_id])){
            unset ($this->children[$item_id]);
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function isEmpty()
    {
        return is_null($this->data);
    }


    /**
     * @return self[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return self
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function getFilter()
    {
        return $this->filter;
    }

    function __get($name)
    {
        switch ($name) {
            case 'id':      return $this->getId();
            case 'data':    return $this->getData();
            case 'children':return $this->getChildren();
            case 'parent':  return $this->getParent();
            case 'filter':  return $this->getFilter();
        }
        throw new \Exception('Property '. $name.' does not exists in TreeItem!');
    }


}
