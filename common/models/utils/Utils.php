<?php

namespace common\models\utils;

use yii\base\Model;
use yii\db\Exception;
use yii\db\Expression;

class Utils
{

    public static function getTimeZone()
    {
//        return new \DateTimeZone('UTC');
        return new \DateTimeZone( \Yii::$app->formatter->timeZone);
    }

    /**
     * Multi implode version
     * @param $sep
     * @param $array
     * @return string
     */
    public static function implode($sep, $array)
    {
        $_array = [];
        if(!is_array($array)) $_array[] = $array;
        else
            foreach($array as $val)
                $_array[] = is_array($val)? self::implode($sep, $val) : $val;
        return implode($sep, $_array);
    }

    public static function getModelErrorsString(Model $model)
    {
        return self::implode('; ', $model->errors);
    }

    public static function addFlashSuccess($message)
    {
        \Yii::$app->session->addFlash('success', $message);
    }

    public static function addFlashError($message)
    {
        \Yii::$app->session->addFlash('error', $message);
    }

    public static function getTimeOffsetStr()
    {
        $date = self::getNow();
        $offset = $date->getOffset();
        $sign = $offset > 0 ? 1 : -1;
        $offset = abs($offset);
        $s = $offset % 60;
        $offset = floor($offset / 60);

        $m = $offset % 60;
        $offset = floor($offset / 60);

        $h = $offset % 60;
        $offset = floor($offset / 60);

        $interval = new \DateInterval('PT'.$h.'H'.$m.'M'.$s.'S');
        if ($sign < 0) {
            $interval->invert = 1;
        }
        $str = $interval->format('%R%H:%I');
        return $str;
    }

    public static function getNow($convert_tz = true)
    {
        $time = new \DateTime(null, new \DateTimeZone('UTC'));
        if ($convert_tz) {
            $time->setTimezone(self::getTimeZone());
        }
        return $time;
    }

    public static function getTimeFromStr($time_str)
    {
        if ($time_str == null) return null;
        return new \DateTime($time_str, self::getTimeZone());
    }

    public static function getTimeForDB($time = null)
    {
        if ($time == null) {
            $time = self::getNow();
        }
        $time->setTimezone(new \DateTimeZone('UTC'));

        $time_str = $time->format('Y-m-d H:i:s');

        return new Expression("TO_TIMESTAMP('$time_str', 'YYYY-MM-DD HH24:MI:SS')");
    }

    /**
     * @param $time_field_value
     * @param bool $convert_timezone
     * @return \DateTime|null
     */
    public static function getTimeFromDB($time_field_value, $convert_timezone = true)
    {
        if ($time_field_value === null){
            return null;
        } elseif (is_a($time_field_value, Expression::class)) {

            $str = $time_field_value->expression;
            $str = str_replace('TO_TIMESTAMP(\'', '', $str);
            $str = str_replace('\', \'YYYY-MM-DD HH24:MI:SS\')', '', $str);
            $dt = new \DateTime($str, new \DateTimeZone('UTC'));

            return $dt;
        } elseif (is_a($time_field_value, \DateTime::class)) {
            return $time_field_value;
        } else {
            $dt = new \DateTime($time_field_value, new \DateTimeZone('UTC'));
            if ($convert_timezone) {
                $dt->setTimezone(self::getTimeZone());
            }
            return $dt;
        }
    }

    public static function showTime($time, $part = 'date_time')
    {
        if (is_string($time)){
            $time = self::getTimeFromDB($time);
        }
        if (!$time){
            return '';
        }
        $time->setTimezone(new \DateTimeZone( \Yii::$app->timeZone));

        if ($part == 'date') {
            return $time->format('d.m.Y');
        } elseif ($part == 'time') {
            return $time->format('H:i:s');
        } elseif ($part == 'time_short') {
            return $time->format('H:i');
        } else {
            return $time->format('d.m.Y, H:i:s');
        }
    }

    public static function time($time)
    {
//        if (is_string($time)){
//            return date_create_from_format('Y-m-d H:i:s', $time);
//        }
//        return $time;
        return self::getTimeFromDB($time);
    }

    /**
     * Для получения полей внутри объекта.. можно с помощью / если поля вложенные
     * Вытягивает и массивы
     * @param $field
     * @return null
     */
    public static function getVal($obj, $field)
    {
        if (empty($obj))return null;

        if (is_string($field)) {
            $f_arr = explode('/', $field);
        } elseif (is_array($field)) {
            $f_arr = $field;
        }
        $f = array_shift($f_arr);
        while ($f){

            if (is_object($obj)) {
                if (property_exists($obj, $f)) {
                    $obj = $obj->$f;
                    if (count($f_arr) && is_array($obj)) {
                        $res = [];
                        foreach ($obj as $item) {
                            $res[] = self::getVal($item, $f_arr);
                        }

                        return $res;
                    }
                } else {
                    return null;
                }
            } elseif (is_array($obj)){
                if (array_key_exists($f, $obj)){
                    $obj = $obj[$f];
                    // не поддерживается получение массива, если наситель - не объект а массив
                } else {
                    return null;
                }

            } else return null;

            $f = array_shift($f_arr);
        }
        return $obj;
    }


    public static function linesToArr($lines)
    {
        $arr = explode("\n", $lines);
        $res = [];
        foreach ($arr as $item){
            $item = trim($item);
            if (!empty($item)) {
                $res[] = $item;
            }
        }
        return $res;
    }

    public static function arrToLines($arr)
    {
        return implode("\n", $arr);
    }


    /**
     * Load models from $data to array
     *
     * @param $data - array params (ex request::post())
     * @param $class_name - loading models class
     * @param null $index_attr - attr, which can be used for set as key in resulting assoc array
     * @return array|null       - result array or null, if no data at all
     *
     * @throws \yii\base\InvalidConfigException
     */
    public static function loadRowsToModels($data, $class_name, $index_attr = null, $empty_arr_on_null = true)
    {
        /** @var Model $obj */
        $obj = new $class_name;
        $form_name = $obj->formName();

        $items = null;
        if (isset($data[$form_name])){

            $items = [];
            $data_arr = $data[$form_name];
            foreach ($data_arr as $i => $row_data){
                /** @var Model $item */
                $item = new $class_name;
                if ($item->load($row_data, '')){
                    if ($index_attr && isset($item->$index_attr)){
                        $items[$item->$index_attr] = $item;
                    } else {
                        $items[] = $item;
                    }

                }
            }
        }
        if (is_null($items) && $empty_arr_on_null){
            $items = [];
        }
        return $items;
    }


    public static function fillArr($arr)
    {
        $res = [];
        if (count($arr)) {
            $max_index = max(array_keys($arr));

            for ($i = 0; $i <= $max_index; $i++) {
                $res[] = isset($arr[$i]) ? $arr[$i] : '';
            }
        }
        return $res;
    }

    public static function setDatePart(\DateTime $dt, $date_part)
    {
        $date = Utils::getTimeFromStr($date_part);
        $y = $date->format("Y");
        $m = $date->format("m");
        $d = $date->format("d");
        $dt->setDate($y, $m, $d);
    }
    public static function setTimePart(\DateTime $dt, $time_part)
    {
        $time = Utils::getTimeFromStr($time_part);
        $time->setTimezone(new \DateTimeZone('UTC'));
        $h = $time->format("H");
        $m = $time->format("i");
        $s = $time->format("s");
        $dt->setTime($h, $m, $s);
    }

    public static function getAuthTokenFromHeaders()
    {
        $request = \Yii::$app->request;
        $headerAuthData = $request->headers->get('Authorization');
        $arr = explode(' ', $headerAuthData);
        if (isset($arr[1])) {
            return trim($arr[1]);
        } else {
            return null;
        }
    }

}