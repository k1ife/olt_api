<?php

namespace common\models\utils;


use yii\helpers\ArrayHelper;

class Tree
{
    /** @var TreeItem[] $root_items */
    protected $root_items = [];

    /** @var TreeItem[] $all_items */
    protected $all_items = [];

    public function isEmpty()
    {
        return !count($this->all_items);
    }

    /**
     * @param ITreeObject[] $objects - pure objects or arrays ['id', 'parent_id', 'object'] for different object classes
//     * @param string $id_attr
//     * @param string $parent_attr
//     * @param string $object_attr
     */
//    public function makeTree(array $objects, $id_attr = 'id', $parent_attr = 'parent_id', $object_attr = 'object')
    public function makeTree(array $objects) : void
    {
        foreach ($objects as $object){
            $this->addItem($object->getId(), $object, null, $object->getParentId());
        }
        $this->removeAllEmptyRootItems();
    }

    protected function getParentItemForAdding($parent)
    {
        $parent_item = null;
        $parent_id = null;
        if ($parent) {
            if (is_a($parent, TreeItem::class)) {
                $parent_item = $parent;
            } else {
                $parent_id = $parent;

                if (!array_key_exists($parent_id, $this->all_items)){
                    $parent_item = new TreeItem($parent_id, null, null, null);
                    $this->root_items[$parent_id] = $parent_item;
                    $this->all_items[$parent_id] = $parent_item;
                } else {
                    $parent_item = $this->all_items[$parent_id];
                }
            }
        }
        return $parent_item;
    }

    public function addRootItem($id, $data, $filter = null)
    {
        return $this->addItem($id, $data, $filter);
    }

    /**
     * @param $id
     * @param $data
     * @param $filter
     * @param null $parent
     * @return TreeItem
     */
    public function addItem($id, $data, $filter, $parent = null)
    {
        if (isset($this->all_items[$id])){
            // item has been added earle
            $item = $this->all_items[$id];
            if ($item->isEmpty()){
                // item is empty - only id has been added for link children
                // write data to it
                $this->all_items[$id]->setData($data);
                // if it has parent - link them
                if ($parent && !$item->parent) {
                    $parent_item = $this->getParentItemForAdding($parent);
                    if ($parent_item){
                        $this->all_items[$parent_item->id]->addChild($this->all_items[$id]);
                        unset ($this->root_items[$id]);
                    }
                }
            }
            return $this->all_items[$id];
        }

        $parent_item = $this->getParentItemForAdding($parent);

        $new_item = new TreeItem($id, $data, $filter, $parent_item);
        if (is_null($parent_item)){
            $this->root_items[$new_item->id] = $new_item;
        }

        $this->all_items[$new_item->id] = $new_item;
        return $new_item;
    }

    /**
     * @param $id
     * @return null|TreeItem
     */
    public function getItem($id)
    {
        if (isset($this->all_items[$id])){
            return $this->all_items[$id];
        } else {
            return null;
        }
    }

    public function removeAllEmptyRootItems()
    {
        $add_to_root_items = [];
        foreach ($this->root_items as $root_item){
            if ($root_item->isEmpty()){
                $add_to_root_items = ArrayHelper::merge($add_to_root_items, $root_item->getChildren());
                unset ($this->root_items[$root_item->id]);
            }
        }

        if (count($add_to_root_items)) {
            $this->root_items = ArrayHelper::merge($this->root_items, $add_to_root_items);
            // make recursion;
            $this->removeAllEmptyRootItems();
        }
    }

    public function sortRootItems($cmp_function)
    {
        usort($this->root_items, function($item_a, $item_b) use ($cmp_function){
            /** @var TreeItem $item_a */
            /** @var TreeItem $item_b */
            return $cmp_function($item_a->getData(), $item_b->getData());
        });
    }

    /**
     * @return TreeItem[]
     */
    public function getRootItems()
    {
        return $this->root_items;
    }

    public function getAllParentsIds($id, $include_itself = true)
    {
        $array = [];

        $item = $this->getItem($id);
        if ($item){
            if ($include_itself){
                $array[$id] = $id;
            }
            do {
                $item = $item->getParent();
                if ($item){
                    $id = $item->getId();
                    $array[$id] = $id;
                }
            } while ($item);
        }

        return $array;
    }

    public function filter()
    {
        foreach ($this->all_items as $item){

            // take elements only, wo children
            while ($item && empty($item->children)){

                $filter_value = $item->filter;
                if (empty($filter_value)){
                    $parent = $item->getParent();
                    $item_id = $item->id;
                    if ($parent) {
                        $parent->deleteChild_internal($item_id);
                    } else {
                        unset ($this->root_items[$item_id]);
                    }
                    unset ($this->all_items[$item_id]);
                }
                $item = $parent;
            }
        }
    }

    public function getAllItemsIds()
    {
        $ids = ArrayHelper::getColumn($this->all_items, function($item){return $item->getId();});
        return $ids;
    }

    function __destruct()
    {
        foreach ($this->root_items as $k => $item){
            unset ($this->root_items[$k]);
        }
        unset ($this->root_items);

        foreach ($this->all_items as $k => $item){
            unset ($this->all_items[$k]);
        }
    }

    public function count()
    {
        return count($this->all_items);
    }

    /**
     * @param TreeItem $item
     * @param TreeItem[] $parents
     */
    public function moveItemToAnotherBranches(TreeItem $item, array $parents)
    {
        $isFirst = true;
        foreach ($parents as $parent) {
            if ($isFirst) {
                // move origin
                $parent->addChild($item);
                if (isset($this->root_items[$item->id])) {
                    // remove from root (if was root)
                    unset($this->root_items[$item->id]);
                }
                $isFirst = false;
            } else {
                // create a copy
                $this->addItem($item->id, $item->getData(), $item->filter, $parent);
            }

        }
    }

}