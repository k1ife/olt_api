<?php

namespace common\models\utils;


use alexBond\thumbler\Thumbler;
use common\models\data\Department;
use common\models\data\Region;
use common\models\data\User;
use yii\base\ErrorException;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageManager
{

    const THUMB_BIG = 'big';
    const THUMB_PROFILE = 'profile';
    const THUMB_SMALL = 'small';
    const THUMB_TALE = 'tale';


    public static function getThumb($img, $type, $empty)
    {
        if (empty($img)){
            $img = $empty;
        }

        if (!$img) {
            return null;
        }

        if (strtolower(substr($img, -3)) == 'svg') {
            // svg returns as is
            return \Yii::getAlias("@imgUrl")."/". $img;
        }

        $method = Thumbler::METHOD_BOXED;
        switch ($type){
            case self::THUMB_BIG:
                $width = 300;
                $height = 300;
                break;

            case self::THUMB_SMALL:
                $width = 50;
                $height = 50;
                break;

            case self::THUMB_PROFILE:
                $width = 88;
                $height = 88;
                break;

            case self::THUMB_TALE:
                $width = 100;
                $height = 100;
                break;

            default:
                $width = 50;
                $height = 50;
        }

        try {
            $path = \Yii::$app->thumbler->resize($img, $width, $height, $method);
        } catch(\Exception $e){
            try {
                $img = $empty;
                $path = \Yii::$app->thumbler->resize($img, $width, $height, $method);
            } catch(\Exception $e){
                return null;
            }
        }

        return str_replace('\\','/', \Yii::getAlias('@thumbsUrl').'/'. $path);
    }

    public static function getBaseUrl($model_or_name)
    {
        // ignoring model - all images in one base folder
        return \Yii::getAlias('@imgUrl').DIRECTORY_SEPARATOR;
    }

    public static function getBasePath($model_or_name)
    {
        // ignoring model - all images in one base folder
        return \Yii::getAlias('@imgPath').DIRECTORY_SEPARATOR;
    }

    public static function uploadAndReplace(Model $model, $upload_attribute, $save_attribute)
    {
        $file = UploadedFile::getInstance($model, $upload_attribute);
        if($file){

            $path = \Yii::getAlias('@imgPath');
            if($model->{$save_attribute}) {
                $file_path = $path.$model->{$save_attribute};
                try{unlink($file_path);}
                catch(\ErrorException $e){}
            }
            $file_name = \Yii::$app->security->generateRandomString() . '.' . $file->extension;
            $pref = strtolower(substr($file_name, 0, 1));
            if (!file_exists($path .'/'.$pref)) {
                mkdir($path.'/'.$pref, 0777, true);
            }

            $file_name = $pref.'/'.$file_name;

            $model->{$save_attribute} = $file_name;
            $file_path = $path.$model->{$save_attribute};

            if(!$file->saveAs($file_path, false)) {
                throw new ErrorException('Can not save file "' . $file->name . '"');
            }
        }
    }

    public static function getUserThumb(User $user, $type)
    {
        return ImageManager::getThumb($user->photo, $type, 'empty_user.png');
    }

    public static function getDepThumb(Department $department)
    {
        return ImageManager::getThumb($department->image, ImageManager::THUMB_TALE, null);
    }

    public static function getRegionThumb(Region $region)
    {
        return ImageManager::getThumb($region->image, ImageManager::THUMB_TALE, null);
    }

        public static function generateFileNameWithPrefix($path, $file_name, $file_ext = null)
    {
        $now = new \DateTime();

        //$pref = strtolower(\Yii::$app->security->generateRandomString(1));
        $pref = $now->format('Y/m');
        if (!file_exists($path .'/'.$pref)) {
            mkdir($path.'/'.$pref, 0777, true);
        }

        $q_pos = strpos($file_name, '?');
        if ($q_pos !== false){
            $file_name = substr($file_name, 0, $q_pos);
        }

        if ($file_name === null){
            // no file name
            $new_name = $now->format('Y-m-d-H-m-s-').strtolower(\Yii::$app->security->generateRandomString(3));
            $file_name = $new_name . ($file_ext ? ('.'.$file_ext) : '');
        }

        $file_name = $pref . '/' . $file_name;
        return $file_name;
    }
} 