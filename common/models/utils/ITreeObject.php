<?php

namespace common\models\utils;

interface ITreeObject
{
    public function getId();
    public function getParentId();

}