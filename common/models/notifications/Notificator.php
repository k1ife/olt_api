<?php

namespace common\models\notifications;

use common\models\data\Project;
use common\models\data\User;
use common\models\dict\Role;
use yii\helpers\Url;

class Notificator
{

    public static function getFromMail()
    {
        return \Yii::$app->params['adminEmail'];
    }

    public static function onCreateUser(User $user)
    {
        $mailer = \Yii::$app->mailer;

        $activation_link = Url::to(['/account/activate', 'token' => $user->activation_token], true);
        $subject = \Yii::t('msg', 'subj_new_user');

        return $mailer->compose('new-user-html', ['user' => $user, 'activation_link' => $activation_link])
            ->setFrom(self::getFromMail())
            ->setTo($user->email)
            ->setSubject($subject)
            ->send();
    }

    public static function onInviteUser(User $user)
    {
        $mailer = \Yii::$app->mailer;

        $activation_link = Url::to(['/account/invited', 'token' => $user->activation_token], true);
        $login_link = Url::to(['account/login'], true);

        $subject = \Yii::t('msg', 'subj_invite_user');

        return $mailer->compose('invite-user-html', [
            'user' => $user,
            'invite_link' => $activation_link,
            'login_link' => $login_link
        ])
            ->setFrom(self::getFromMail())
            ->setTo($user->email)
            ->setSubject($subject)
            ->send();
    }

    public static function onActivateUser(User $user)
    {
        $mailer = \Yii::$app->mailer;

        $subject = \Yii::t('msg', 'subj_activation');

        return $mailer->compose('new-user-activation-html', ['user' => $user])
            ->setFrom(self::getFromMail())
            ->setTo($user->email)
            ->setSubject($subject)
            ->send();
    }

    public static function onResetPassword(User $user)
    {
        $mailer = \Yii::$app->mailer;

        $password_reset_link = Url::to(['/account/reset-password', 'token' => $user->password_reset_token], true);
        $subject = \Yii::t('msg', 'subj_password_reset_request');

        return $mailer->compose('password-reset-request-html', ['user' => $user, 'password_reset_link' => $password_reset_link])
            ->setFrom(self::getFromMail())
            ->setTo($user->email)
            ->setSubject($subject)
            ->send();
    }

    public static function onChangeProjectRole(User $user, Project $project, Role $old_role, Role $new_role)
    {
        $mailer = \Yii::$app->mailer;

        $subject = \Yii::t('msg', 'subj_change_role');

        return $mailer->compose('change-project-role-html', [
                'user' => $user, 'project' => $project,
                'old_role_name' => $old_role->name,
                'new_role_name' => $new_role->name
            ])
            ->setFrom(self::getFromMail())
            ->setTo($user->email)
            ->setSubject($subject)
            ->send();
    }

    public static function testMail($email)
    {
        $mailer = \Yii::$app->mailer;


        return $mailer->compose('test-html')
            ->setFrom(self::getFromMail())
            ->setTo($email)
            ->setSubject('Test mail')
            ->send();
    }

}