<?php

namespace common\models\notifications;


use common\models\data\EventLog;
use common\models\data\Project;
use common\models\data\User;
use yii\helpers\ArrayHelper;

class DailyMail
{

    /**
     * @var array $users_data
     * format:
     * [<user_id> => [
     *      'user' => User,
     *      'projects' => [
     *              <project_id> => [
     *                  'project' => Project,
     *                  'events' => [
     *                      [
     *                          'event' => ProjectLog,
     *                          'text' => <event text>
     *                      ],
     *                      ...
     *                  ]
     *              ],
     *              ...
     *          ]
     *      ],
     *      ...
     * ]
     */
    private $users_data = [];
    private $users_by_projects = [];
    private $events_ids = [];
    private $logs = [];

    private $send_counter = 0;
    private $error_counter = 0;

    public function doMail()
    {
        $this->fillData();
        $this->sendEmails();
        $this->logs[] = "Send successfully: $this->send_counter";
        $this->logs[] = "Send errors: $this->error_counter";


        EventLog::updateAll(
            ['send_notification' => EventLog::SEND_NOTIFICATION_SEND_OK],
            ['id' => $this->events_ids]);
        return $this->logs;
    }

    private function fillData()
    {
        $query = EventLog::find()
            ->where(['send_notification' => EventLog::SEND_NOTIFICATION_TO_SEND])
        //TODO now take only project's notifications
            ->andWhere(['not', ['project_id' => null]])

            ->orderBy(['time' => SORT_ASC]);

        /** @var EventLog[] $all_events */
        $all_events = $query->all();
        foreach ($all_events as $event) {
            $this->events_ids[] = $event->id;
            $project = $event->project;
            if ($project) {
                $users = $this->getUsers($project);
                $text = $event->getNotificationText();
                foreach ($users as $user) {
                    $this->addUserNotificationData($user, $project, $event, $text);
                }
            }
        }
    }

    private function addUserNotificationData(User $user, $project, EventLog $event, $text)
    {
        if (!array_key_exists($user->id, $this->users_data)) {
            $this->users_data[$user->id] = [
                'user' => $user,
                'projects' => []
            ];
        }
        $user_projects = $this->users_data[$user->id]['projects'];
        if (!array_key_exists($project->id, $user_projects)) {
            $user_projects[$project->id] = [
                'project' => $project,
                'events' => []
            ];
        }
        $user_events = $user_projects[$project->id]['events'];
        if (!array_key_exists($event->id, $user_events)){
            $user_events[$event->id] = [
                'event' => $event,
                'text' => $text
            ];
        }
        $user_projects[$project->id]['events'] = $user_events;
        $this->users_data[$user->id]['projects'] = $user_projects;
    }

    private function getUsers(Project $project)
    {
        if (!array_key_exists($project->id, $this->users_by_projects)) {
            $users = ArrayHelper::merge(
                    $this->getProjectLeadersToSend($project),
                    $this->getAdminsToSend());

            $this->users_by_projects[$project->id] = $users;
        } else {
            $users = $this->users_by_projects[$project->id];
        }
        return $users;
    }

    /**
     * @return User[]
     */
    protected function getAdminsToSend()
    {
        $admins = User::getAdmins();
        $users = [];
        foreach ($admins as $admin) {
            $users[$admin->id] = $admin;
        }
        return $users;
    }

    /**
     * Returns array of User - leader current project and all it's parents
     * Only Users with 'send_project_notifications' flag set
     * Recursive function
     *
     * @param Project $project
     * @return User[]
     */
    private function getProjectLeadersToSend($project)
    {
        $users = [];
        $leaders = $project->leaders;
        foreach ($leaders as $leader) {
            if ($leader && $leader->send_project_notifications && $leader->isActivated()) {
                $users[$leader->id] = $leader;
            }
        }
        $parent_project = $project->parent;
        if ($parent_project) {
            $users = ArrayHelper::merge($users, $this->getProjectLeadersToSend($parent_project));
        }
        return $users;
    }

    private function sendEmails()
    {
        $mailer = \Yii::$app->mailer;
        $from = Notificator::getFromMail();
        $today = new \DateTime();
        $today_str = $today->format('d.m.Y');
        $mail_subj = "Oppdatering fra Den Olympiske Løypa $today_str";

        foreach ($this->users_data as $user_data) {
            $user = $user_data['user'];
            $user_projects = $user_data['projects'];

            $res = $mailer->compose('daily-mail-html', [
                    'user' => $user,
                    'projects_data' => $user_projects])
                ->setFrom($from)
                ->setTo($user->email)
                ->setSubject($mail_subj)
                ->send();
            if (!$res) {
                $this->logs[] = "Error send to '".$user->email."'";
                $this->error_counter++;
            } else {
                $this->send_counter++;
            }
        }
    }
}