<?php
namespace common\models\auth;

use common\models\data\User;
use common\models\utils\Utils;
use yii\helpers\ArrayHelper;

class JWT
{
    public $userId;
//    public $email;


    /**
     * JWT constructor.
     * @param User|string $param
     */
    public function __construct($param)
    {
        if (is_string($param)) {

            $token = $param;
            try {
                $payload = (array)\Firebase\JWT\JWT::decode($token, \Yii::$app->params['jwtKey'], array('HS256'));
            } catch (\Exception $e) {
                $payload = [];
            }
            $this->userId = ArrayHelper::getValue($payload, 'id');

        } elseif (is_a($param, User::class)) {

            $this->userId = $param->id;
//            $this->email = $param->email;
        }

    }

    public function token()
    {
        $payload = [
            'id' => $this->userId,
//            'email' => $this->email,
            'iat' => time(),
            'exp' => time() + (2 * 7 * 24 * 60 * 60)
        ];
        return \Firebase\JWT\JWT::encode($payload, \Yii::$app->params['jwtKey']);
    }

    public static function createToken(User $user)
    {
        $jwt = new JWT($user);
        return $jwt->token();
    }
}