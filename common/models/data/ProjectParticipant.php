<?php


namespace common\models\data;

use common\models\dict\Subrole;
use yii\db\ActiveRecord;

/**
 * Class ProjectParticipant
 * @package common\models\data
 *
 *
 * @property string $id [integer]
 * @property string $project_id [integer]
 * @property string $user_id [integer]
 * @property string $subrole_id [integer]
 *
 * @property User $user
 * @property Project $project
 * @property Subrole $subrole
 */
class ProjectParticipant extends ActiveRecord
{
    public static function tableName()
    {
        return 'projects_participants';
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['project_id', 'integer'],
            ['user_id', 'integer'],
            ['subrole_id', 'integer'],
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    public function getSubrole()
    {
        return $this->hasOne(Subrole::class, ['id' => 'subrole_id']);
    }

    public static function add($project_id, $user_id, $sub_role_id)
    {
        $new = new static();
        $new->project_id = $project_id;
        $new->user_id = $user_id;
        $new->subrole_id = $sub_role_id;
        $new->save();
        return $new;
    }
}