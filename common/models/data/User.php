<?php
namespace common\models\data;

use common\models\dict\Role;
use common\models\utils\Utils;
use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use common\models\notifications\Notificator;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property string $password write-only password
 * @property string $first_name
 * @property string $last_name
 * @property string $photo
 * @property string $profession
 * @property string $activation_token
 * @property string $ga_secret
 * @property string $deleted_at
 * @property boolean $send_project_notifications
 * @property string $access_token
 * @property string $phone
 * @property int $role [smallint]
 * @property bool $need_change_password [boolean]
 *
 *
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_IN_REGISTRATION = 5;
    const STATUS_INVITED = 6;
    const STATUS_ACTIVE = 10;

    const SCENARIO_INVITE = 'invite';

    const IGNORE_2FA = 'not use';


    public static function getStatusesLabels()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
            self::STATUS_INVITED => Yii::t('app', 'Invited'),
            self::STATUS_DELETED => Yii::t('app', 'Deleted'),
            self::STATUS_IN_REGISTRATION => Yii::t('app', 'In registration')
        ];
    }

    public function getStatusLabel()
    {
        $statuses = self::getStatusesLabels();
        if (isset($statuses[$this->status])){
            return $statuses[$this->status];
        } else {
            return '?';
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
//            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],

            ['first_name', 'string'],
            ['last_name', 'string'],
            ['email', 'email'],
            [['first_name', 'last_name', 'email'], 'required', 'on' => self::SCENARIO_DEFAULT],
            [['email'], 'required', 'on' => self::SCENARIO_INVITE],
            ['email', 'unique',
                'targetClass' => User::class,
                'filter' => ['not', ['id' => $this->id]],
                'message' => 'EmailAlreadyTaken'],

            ['phone', 'string'],
            ['profession', 'string'],
            ['ga_secret', 'string'],
            ['deleted_at', 'safe'],
            ['send_project_notifications', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [

            'first_name' => Yii::t('app', 'First name'),
            'last_name' => Yii::t('app', 'Last name'),
            'email' => Yii::t('app','Email'),
            'password' => Yii::t('app','Password'),
            'phone' => Yii::t('app', 'Phone'),
            'profession' => Yii::t('app', 'Profession'),
            'send_project_notifications' => Yii::t('app', 'Project notifications')

        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);
        $sameEmail = User::findByEmail($this->email);
        if ($sameEmail && $sameEmail->id != $this->id) {
            $this->addError('email', 'This email address has already been taken.');
        }
        return !$this->hasErrors();
    }


    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::find()
            ->where(['id' => $id])
            ->andWhere(['not', ['status' => self::STATUS_DELETED]])
            ->limit(1)
            ->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }


    /**
     * @param $email
     * @param $only_active
     * @return null|static
     */
    public static function findByEmail($email, $only_active = true)
    {
        $query = static::find()
            ->where(['ilike', 'email', $email, false]);

        if ($only_active){
            $query->andWhere(['status' => self::STATUS_ACTIVE]);
        }
        return $query->one();
    }

    /**
     * @param $email
     * @return User|null
     */
    public static function findByEmailAndPassword($email, $password)
    {
        $user = static::findByEmail($email);
        if ($user) {
            $passwordHash = $user->password_hash;
            if (User::validatePassword($password, $passwordHash)) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @param $id
     * @return null|static
     */
    public static function findById($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public static function validatePassword($password, $passwordHash)
    {
        return Yii::$app->security->validatePassword($password, $passwordHash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
        $this->need_change_password = false;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function generateActivationToken()
    {
        $this->activation_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function removeActivationToken()
    {
        $this->activation_token = null;
    }


    public static function createNewUser($email, $password, $first_name, $last_name, $send_email = true, $need_change_password = false)
    {
        $user = new User();
        $user->email = $email;
        $user->role = Role::ROLE_USER;
        $user->status = self::STATUS_IN_REGISTRATION;
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->ga_secret = self::IGNORE_2FA; // switch off use 2 factor auth
        $user->setPassword($password);
        $user->need_change_password = $need_change_password;
        $user->generateAuthKey();
        $user->generateActivationToken();

        if ($user->save() && $send_email){
            Notificator::onCreateUser($user);
        }
        return $user;
    }

    public static function inviteNewUser($email, $role, $can_repeat_invitation)
    {
        // check if user already created
        $user = User::findByEmail($email, false);
        $prev_status = null;
        if ($user){
            // user already exists
            $prev_status = $user->status;

            // if role less then new role - make it greater
            if ($user->role < $role){
                $user->role = $role;
            }

            if ($user->isActivated()) {
                // if was activated - add error
                $user->addError('email', Yii::t('app', 'User with email {0} already registered. Invitation not will be send', $user->email));
            } elseif ($user->status == User::STATUS_DELETED) {
                // if was deleted - undelete him
                $user->status = User::STATUS_INVITED;
                $user->generateAuthKey();
            } else {
                // send invitation again if param $can_repeat_invitation is true
                if (!$can_repeat_invitation) {
                    $user->addError('email', Yii::t('app', 'User with email {0} already invited. Left email to repeat invitation', $user->email));
                }
            }
            $user->scenario = self::SCENARIO_INVITE;
        } else {
            // not found user with such email
            $user = new User(['scenario' => self::SCENARIO_INVITE]);
            $user->email = $email;
            $user->role = $role;
            $user->first_name = '';
            $user->last_name = '';
            $user->status = User::STATUS_INVITED;
            $user->ga_secret = self::IGNORE_2FA; // switch off use 2 factor auth
            $user->generateAuthKey();
        }

        if (!$user->hasErrors()) {
            if (!$user->activation_token) {
                $user->generateActivationToken();
            }

            if ($user->save()) {
                if (!Notificator::onInviteUser($user)) {
                    // error send invitaion - delete user or roll back him state
                    if ($prev_status === null) {
                        $user->delete();
                    } else {
                        $user->status = $prev_status;
                        $user->save();
                    }
                    $user->addError('email', Yii::t('app', 'Can not send message to email: {0}', $user->email));
                }
            }
        }
        return $user;
    }


    /**
     * @param $token
     * @return null|static
     */
    public static function findByActivationToken($token)
    {
        return User::findOne(['activation_token' => $token]);
    }



    public function activate($send_email = true)
    {
        $this->status = self::STATUS_ACTIVE;
        $this->activation_token = null;
        if ($this->save(false)){
            if ($send_email) {
                Notificator::onActivateUser($this);
            }
            return true;
        }
        return false;
    }

    public function getUserNameLabel()
    {
        $full_name = $this->getFullName();
        return $full_name ? $full_name : $this->email;
    }

    public function getFullName()
    {
        return trim($this->first_name.' '.$this->last_name);
    }

    public static function isGuest()
    {
        return !isset(Yii::$app->components['user']) || Yii::$app->user->isGuest;
    }

    public static function isCurrAdmin()
    {
        $user = self::getCurrent();
        if ($user && $user->isSuperAdmin()) {
            return true;
        }
        return false;
    }

    public function isDeleted()
    {
        return $this->status == User::STATUS_DELETED;
    }

    /**
     * @return User
     */
    public static function getCurrent()
    {
        if (self::isGuest()){
            return null;
        }
        return Yii::$app->user->identity;
    }

    /**
     * Возвращает либо пользователя переданного в параметре,
     * либо текущего (если был передал null
     *
     * @param User|null $user
     * @return null|User
     */
    public static function curr(User $user = null)
    {
        try {
            return $user ?? User::getCurrent();
        } catch (\Exception $e) {
            return null;
        }
    }

    public static function getCurrentId()
    {
        if (self::isGuest()){
            return null;
        }
        return Yii::$app->user->id;
    }

    public function isSuperAdmin()
    {
        foreach (UserRoles::getUserRoles($this) as $role) {
            if ($role->role_code == Role::ROLE_ADMIN) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return User[]
     */
    public static function getAllActive()
    {
        return self::find()
            ->where(['status' => self::STATUS_ACTIVE])
            ->orderBy(['last_name' => SORT_ASC, 'first_name' => SORT_ASC])
            ->all();
    }

    public function isLeaderAnywhere()
    {
        return ProjectSearch::countProjectsAsLeaderIn($this->id) > 0;
    }

//    public function isLeaderNotAdmin()
//    {
//        return !$this->isSuperAdmin() && $this->isLeaderAnywhere();
//    }

    public static function countAdmins()
    {
        return User::find()
            ->where(['role' => Role::ROLE_ADMIN])
            ->andWhere(['status' => User::STATUS_ACTIVE])
            ->count();
    }

    public static function getAdmins()
    {
        return User::find()
            ->where(['role' => Role::ROLE_ADMIN])
            ->andWhere(['status' => User::STATUS_ACTIVE])
            ->all();
    }

    public function beforeSave($insert)
    {
        if ($insert){
            $this->created_at = Utils::getTimeForDB();
        }
        return parent::beforeSave($insert);
    }

    public function hasPassword()
    {
        return !empty($this->password_hash);
    }

    public function isActivated()
    {
        return $this->status == User::STATUS_ACTIVE;
    }

    /**  google */
    public function use2FA()
    {
        if ($this->ga_secret == self::IGNORE_2FA){
            return false;
        }
        return true;
    }

    protected static $_ga = null;
    protected static function getGA()
    {
        if (!self::$_ga){
            self::$_ga = new \PHPGangsta_GoogleAuthenticator();
        }
        return self::$_ga;
    }

    public function get2FASecret()
    {
        if (empty($this->ga_secret) || $this->ga_secret == self::IGNORE_2FA){
            $ga = self::getGA();
            $secret = $ga->createSecret();
            $this->ga_secret = $secret;
            $this->save();
        }

        return $this->ga_secret;
    }

    public function get2FAQRUrl()
    {
        $ga = self::getGA();
        $secret = $this->get2FASecret();
        $qrCodeUrl = $ga->getQRCodeGoogleUrl('DOL-'.$this->email, $secret);
        return $qrCodeUrl;
    }

    public function check2FA($code)
    {
        $ga = self::getGA();
        $secret = $this->get2FASecret();
        $checkResult = $ga->verifyCode($secret, $code, 8);
        return $checkResult;
    }

    public function deleteUser() {
        $this->status = self::STATUS_DELETED;
        $this->deleted_at = Utils::getTimeForDB();
        $this->save(false);
    }

    public function undeleteUser(){
        $this->status = self::STATUS_ACTIVE;
        $this->save(false);
    }

    /**
     * DO not use directly. Only for UserRoles
     * @return \yii\db\ActiveQuery
     */
    public function getUserRoles()
    {
        return $this->hasMany(UserRole::class, ['user_id' => 'id'])
            ->orderBy(['role_code' => SORT_DESC]);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

    }
}
