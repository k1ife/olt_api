<?php
namespace common\models\data;

use yii\db\ActiveRecord;

/**
 * Class Trainer
 * @package common\models\data
 *
 * @property string $id [integer]
 * @property string $dep_id [integer]
 * @property string $name [varchar(255)]
 *
 * @property Department $dep
 */
class Trainer extends ActiveRecord
{
    public static function tableName()
    {
        return 'trainers';
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['dep_id', 'integer'],
            ['name', 'string'],

            [['dep_id', 'name'], 'unique', 'targetAttribute' => ['dep_id', 'name']],
            [['dep_id', 'name'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'dep_id' => \Yii::t('app', 'Department'),
            'name' => \Yii::t('app', 'Name'),
        ];
    }


    public function getDep()
    {
        return $this->hasOne(Department::class, ['id' => 'dep_id']);
    }

    /**
     * process input $ids array
     * if in array presents new string names - they are adding to table
     * and new ids are put to res array
     * @param $ids
     * @param $depId
     * @return array
     */
    public static function processIdsNames($ids, $depId)
    {
        $res = [];
        if (is_array($ids)) {
            foreach ($ids as $id) {
                if (is_numeric($id)) {
                    $res[] = $id;
                } else {
                    $new = new static();
                    $new->dep_id = $depId;
                    $new->name = $id;
                    if (!$new->save()) {
                        $new = static::find()
                            ->where(['ilike', 'name', $id])
                            ->andWhere(['dep_id' => $depId])
                            ->one();
                    }
                    if ($new && $new->id) {
                        $res[] = $new->id;
                    }
                }
            }
        }
        return $res;
    }
}