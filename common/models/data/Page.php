<?php
namespace common\models\data;

use yii\db\ActiveRecord;

/**
 * Class Page
 * @package common\models\data
 *
 *
 * @property string $id [integer]
 * @property string $slug
 * @property string $name
 * @property string $title
 * @property string $description
 * @property bool $in_main_menu [boolean]
 * @property bool $guest_access [boolean]
 * @property string $content
 * @property string $sort [integer]
 *
 *
 */
class Page extends ActiveRecord
{
    public static function tableName()
    {
        return 'pages';
    }

    public function rules()
    {
        return [
            ['id', 'integer'],

            ['slug', 'string'],
            ['slug', 'unique'],
            ['slug', 'required'],

            ['name', 'string'],

            ['title', 'string'],
            ['title', 'required'],

            ['description','string'],
            ['in_main_menu', 'boolean'],
            ['guest_access', 'boolean'],

            ['content', 'string'],
            ['content', 'required'],

            ['sort', 'integer'],
        ];
    }

    /**
     * @return Page[]
     */
    public static function getPagesForMenu($guestAccess = false)
    {
        $query = Page::find()
            ->select(['id', 'slug', 'name', 'guest_access', 'sort'])
            ->where(['in_main_menu' => true])
            ->orderBy(['sort' => SORT_ASC]);
        if ($guestAccess) {
            $query->andWhere(['guest_access' => true]);
        }
        return $query->all();
    }

    /**
     * @param $slug
     * @return Page|null
     */
    public static function findBySlug($slug)
    {
        return Page::findOne(['slug' => $slug]);
    }

}