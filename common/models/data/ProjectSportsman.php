<?php
namespace common\models\data;

use yii\db\ActiveRecord;

/**
 * Class ProjectSportsman
 * @package common\models\data
 *
 * @property string $project_id [integer]
 * @property string $trainer_id [integer]
 */
class ProjectSportsman extends ActiveRecord
{
    public static function tableName()
    {
        return 'project_sportsmen';
    }


}