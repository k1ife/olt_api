<?php

namespace common\models\data;

use common\models\data\base\BaseMasterEntity;
use common\models\dict\Role;
use common\models\utils\ImageManager;
use common\models\utils\Utils;
use common\components\linkerBehavior\LinkerBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Class Department
 * @package common\models\data
 *
 *
 * @property string $id [integer]
 * @property string $name
 * @property int $closed_at [timestamp(0)]
 * @property string $image
 *
 * @property string $min_note_date [date]
 * @property string $max_note_date [date]
 *
 * @property Project[] $projects
 * @property string $caption
 *
 * @property User[] $admins
 * @property integer[] $admins_ids
 */
class Department extends BaseMasterEntity
{

    public function getEntityClass()
    {
        return Department::class;
    }

    static public function getEntityCode()
    {
        return self::ENTITY_DEP;
    }

    static public function getEntityUrl()
    {
        return 'departments';
    }




    static public function getEntityLinkField()
    {
        return 'department_id';
    }


    public static function tableName()
    {
        return "departments";
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['name', 'string'],
            ['closed_at', 'safe'],
            ['isClosed', 'boolean'],
            ['image', 'string'],

            ['min_note_date', 'safe'],
            ['max_note_date', 'safe'],

//            ['admins_ids', 'each', 'rule' => ['integer']],
            ['admins_ids', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t("app", "Name"),
//            "coach_id" => \Yii::t("app", "Coach"),
//            "leader_id" => \Yii::t("app", "Federation Leader"),
            "isClosed" => \Yii::t("app", "Archived"),
            'admins_ids' => \Yii::t('app', 'Department admin'),
        ];
    }

    public function behaviors()
    {
        return [
            'manyToMany' => [
                'class' => LinkerBehavior::class,
                'relations' => [
                    'admins_ids' => [
                        'admins',
                        'updater' => [
                            'viaTableAttributesValue' => [
                                'role_code' => Role::ROLE_DEP_ADMIN
                            ],
                            'viaTableCondition' => [
                                'role_code' => Role::ROLE_DEP_ADMIN
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }

    public function getAdmins()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable(UserRole::tableName(), ['department_id' => 'id'],
            function ($query) {
                $query->andWhere([
                    'role_code' => Role::ROLE_DEP_ADMIN,
                ]);
                return $query;
            });
    }

    /**
     * @return int|string
     */
    public static function countActive()
    {
        return static::find()->where(['closed_at' => null])->count();
    }

    public function getIsClosed()
    {
        return $this->closed_at !== null;
    }

    public function setIsClosed($is_closed)
    {
        if ($is_closed && !$this->closed_at){
            $this->closed_at = Utils::getTimeForDB();

        } elseif (!$is_closed && $this->closed_at){
            $this->closed_at = null;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::class, ['id' => 'project_id'])
            ->viaTable('projects_departments', ['department_id' => 'id'])
            ->andWhere(['closed_at' => null]);
    }

    /**
     * @return \DateTime|null
     */
    public function getCloseDateToShow()
    {
        $close_date = Utils::time($this->closed_at);
        return $close_date;
//        $max_details_date = Utils::time($this->max_details_date);
//        $close_date = Utils::time($this->close_date);
//        $current_date = Utils::getNow();
//        if ($close_date) {
//            $max_date = $max_details_date ? max ($max_details_date, $close_date) : $close_date;
//        } else {
////            $max_date = $max_details_date ? max ($max_details_date, $current_date) : $current_date;
//            $max_date = null;
//        }
//        return $max_date;
    }

    /**
     * @param int $page_size
     * @param int $page
     * @return EventLog[]
     */
    public function getLog($page_size = 500, $page = 1)
    {
        $show_log_code = EventLog::SHOW_LOG_PROJECT_PAGE;
        $offset = ($page-1) * $page_size;
        return EventLog::find()
            ->where(['department_id' => $this->id, 'show_log' => $show_log_code])
            ->orderBy(['time' => SORT_DESC])
            ->limit($page_size)
            ->offset($offset)
            ->all()
            ;
    }

    /**
     * @return Department[]
     */
    public static function getAll()
    {
        return Department::find()
            ->orderBy(['name' => SORT_ASC])
            ->all();
    }

    /**
     * @param User $user
     * @return Department[]
     */
    public static function getAllForUser($user)
    {
        $query = Department::find()->orderBy(['name' => SORT_ASC])->andWhere(['closed_at' => null]);
        if (!$user) {
            $query->andWhere(new Expression('1=0'));
        } elseif (!$user->isSuperAdmin()) {
            $user_id = $user->id;
            $query
                ->leftJoin('users_roles urr', 'urr.department_id = departments.id')
                ->leftJoin('projects_departments pd', 'departments.id = pd.department_id')
                ->leftJoin('users_roles urp', 'urp.project_id = pd.project_id')
                ->andWhere([
                    'or',
                    ['urr.user_id' => $user_id],
                    ['urp.user_id' => $user_id],
                ])
            ;
        }

        return $query->all();
    }

    /**
     * @param User $user
     * @return array [id => name]
     */
    public static function getAllArrForUser($user)
    {
        return ArrayHelper::map(static::getAllForUser($user), "id", "name");
    }

    public function getImageUrl()
    {
        return ImageManager::getDepThumb($this);
    }

    public function getCaption()
    {
        return $this->name;
    }

        /**
     * @param User $user
     * @return bool
     */
    public function canView($user)
    {
        return UserPermissions::can($user, UserPermissions::VIEW, $this);
    }

    /**
     * @param User $user
     * @return boolean
     */
    public static function canAdd($user)
    {
        return UserPermissions::can($user, UserPermissions::ADD, Department::class);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canEdit($user)
    {
        return UserPermissions::can($user, UserPermissions::EDIT, $this);
    }


    /**
     * @param User $user
     * @return bool
     */
    public function canClose($user)
    {
        return UserPermissions::can($user, UserPermissions::DELETE, $this);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canAddNotes($user)
    {
        return UserPermissions::can($user, UserPermissions::ADD_DETAIL, $this);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canAddEvents($user)
    {
       return UserPermissions::can($user, UserPermissions::ADD_EVENT, $this);
    }


}