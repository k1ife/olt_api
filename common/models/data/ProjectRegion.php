<?php


namespace common\models\data;

use yii\db\ActiveRecord;

/**
 * Class ProjectRegion
 * junction entity
 *
 * @package common\models\data
 *
 * @property string $project_id [integer]
 * @property string $region_id [integer]
 * @property int $mode [smallint]
 */
class ProjectRegion extends ActiveRecord
{
    const MODE_NORMAL = 1;      // region set to current project directly
    const MODE_CHILDREN = 2;    // region set to current project by child project

    public static function tableName()
    {
        return 'projects_regions';
    }

    public static function updateForProject($project_id, $regions_modes) {
        ProjectRegion::deleteAll(['project_id' => $project_id]);
        $reg_modes = $regions_modes;
        $data = [];
        foreach ($reg_modes as $region_id => $mode) {
            $data[] = [$project_id, $region_id, $mode];
        }

        \Yii::$app->db->createCommand()->batchInsert(ProjectRegion::tableName(),
            ['project_id', 'region_id', 'mode'], $data)
            ->execute();
    }
}