<?php

namespace common\models\data;

use common\models\data\base\BaseMasterEntity;
use common\models\data\details\Detail;
use common\models\data\details\fields\DetailField;
use common\models\detail_formats\DetailFieldType;
use common\models\detail_formats\DetailFormat;
use common\models\detail_formats\DetailFormatField;
use common\models\dict\ProjectDetailCategory;
use common\models\utils\Utils;
use yii\db\ActiveRecord;

/**
 * Class ProjectLog
 * @property integer $id
 * @property integer $project_id
 * @property integer $department_id
 * @property integer $region_id
 * @property string $time
 * @property integer $user_id
 * @property integer $event_code
 * @property string $params
 * @property integer $send_notification  // send notification code
 * @property string $send_time          // send notification time
 * @property integer $show_log          // showing log event type
 *
 * @property string $description
 *
 * @property Project $project
 * @property User $user
 */

class EventLog extends ActiveRecord
{

    const ACTION_CREATE_PROJECT = 1;
    const ACTION_EDIT_PROJECT = 2;
    const ACTION_CHANGE_CURRENT_POSITION = 10;
    const ACTION_CHANGE_GOAL_POSITION = 11;
    const ACTION_CHANGE_PROJECT_DEPARTMENT = 12;
    const ACTION_CHANGE_PROJECT_CATEGORY = 13;

    const ACTION_SET_LEADER = 20;
    const ACTION_CREATE_HEADING = 30;
    const ACTION_CHANGE_HEADING_TYPE = 31;
    const ACTION_DELETE_HEADING = 32;
    const ACTION_CREATE_DETAIL = 40;
    const ACTION_CHANGE_DETAIL_NAME = 41;
    const ACTION_CHANGE_DETAIL_DATE = 42;
    const ACTION_CHANGE_DETAIL_FORMAT = 43;
    const ACTION_CHANGE_DETAIL_ADD_FILE = 44;
    const ACTION_CHANGE_DETAIL_DELETE_FILE = 45;
    const ACTION_DELETE_DETAIL = 46;
    const EVENT_MILESTONE = 50;
    const EVENT_MEETING = 51;

    const ACTION_CHANGE_OBJ_MAIN = 60;
    const ACTION_ADD_OBJ = 61;
    const ACTION_DEL_OBJ = 62;
    const ACTION_CHNG_OBJ = 63;
    const ACTION_ADD_FOCUS = 65;
    const ACTION_DEL_FOCUS = 66;
    const ACTION_CHNG_FOCUS = 67;

    const SHOW_LOG_NO = 0;                  // do not show event
    const SHOW_LOG_PROJECT_PAGE = 1;        // show event on project page

    const SEND_NOTIFICATION_NO = 0;         // don't send notification
    const SEND_NOTIFICATION_TO_SEND = 1;    // need to send notification
    const SEND_NOTIFICATION_SEND_OK = 2;    // notification was send
//    const SEND_NOTIFICATION_SEND_ERR = 3;

    /**
     * Returns SEND_NOTIFICATION_XX code by ACTION_XX code
     * @param $event_code
     * @return int|mixed
     */
    protected static function getNotificationCodeByEvent($event_code) {

        $arr = [
            self::ACTION_CREATE_PROJECT => self::SEND_NOTIFICATION_NO,
            self::ACTION_EDIT_PROJECT => self::SEND_NOTIFICATION_NO,

            self::EVENT_MEETING => self::SEND_NOTIFICATION_NO,
            self::EVENT_MILESTONE => self::SEND_NOTIFICATION_NO,
        ];

        if (array_key_exists($event_code, $arr)) {
            return $arr[$event_code];
        }
        // SEND_NOTIFICATION_TO_SEND by default
        return self::SEND_NOTIFICATION_TO_SEND;
    }

    protected static function getShowCodeByEvent($event_code) {
        $arr = [
            self::ACTION_EDIT_PROJECT => self::SHOW_LOG_PROJECT_PAGE,
            self::ACTION_CREATE_PROJECT => self::SHOW_LOG_PROJECT_PAGE,
            self::ACTION_CHANGE_CURRENT_POSITION => self::SHOW_LOG_PROJECT_PAGE,
            self::ACTION_CHANGE_GOAL_POSITION => self::SHOW_LOG_PROJECT_PAGE,
            self::EVENT_MEETING => self::SHOW_LOG_PROJECT_PAGE,
            self::EVENT_MILESTONE => self::SHOW_LOG_PROJECT_PAGE
        ];
        if (array_key_exists($event_code, $arr)) {
            return $arr[$event_code];
        }

        return self::SHOW_LOG_NO;
    }


    public static function tableName()
    {
        return 'events';
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['project_id', 'integer'],
            ['department_id', 'integer'],
            ['region_id', 'integer'],
            ['time', 'safe'],
            ['user_id', 'integer'],
            ['event_code', 'integer'],
            ['params', 'string'],

            ['send_notification', 'integer'],
            ['send_time', 'safe'],
            ['show_log', 'integer'],
        ];
    }

    /**
     * @param BaseMasterEntity $master
     * @param $event_code
     * @param $params
     */
    public static function addEvent($master, $event_code, $params)
    {
        $new = new EventLog();
        if (is_a($master, Department::class)) {
            $new->department_id = $master->id;
        } elseif (is_a($master, Region::class)) {
            $new->region_id = $master->id;
        } else {
            $new->project_id = $master->id;
        }

        $new->time = Utils::getTimeForDB();
        $new->user_id = User::getCurrentId();
        $new->event_code = $event_code;
        $new->setParamsArr($params);
        $new->send_time = null;
        $new->show_log = self::getShowCodeByEvent($event_code);
        $new->send_notification = self::getNotificationCodeByEvent($event_code);
        $new->save();

//        switch ($event_code) {
//            case self::ACTION_ADD_OBJ:
//            case self::ACTION_ADD_FOCUS:
//            case self::ACTION_CHANGE_OBJ_MAIN:
//            case self::ACTION_CHNG_OBJ:
//            case self::ACTION_CHNG_FOCUS:
//            case self::ACTION_DEL_OBJ:
//            case self::ACTION_DEL_FOCUS:
//                // create project detail...
//                static::createProjectDetail($master, $new);
//                break;
//        }
    }

    /**
     * @param Project $project
     * @param EventLog $eventLog
     */
    public static function createProjectDetail($project, $eventLog)
    {
        $detailCat = ProjectDetailCategory::findProjectManageCategory();
        if ($detailCat) {
            $format = DetailFormat::findByCode(DetailFormat::CODE_DEFAULT);
            if ($format) {
                $detail = new Detail();
                $detail->category_id = $detailCat->id;
                $detail->format_id = $format->id;
                $detail->project_id = $project->id;
                $detail->name = $eventLog->getTypeLabel();
                $detail->date_time = $eventLog->time;


                $fieldList = $detail->fieldList;
                $fields = $fieldList->getAll();
                foreach ($fields as $field) {
                    if ($field->formatField->field_type === DetailFieldType::TEXT_AREA) {
                        $field->value = $eventLog->getNotificationText();
                    }
                }
                $detail->save();
            }
        }
    }

    public static function addEventTimed($master_class, $master_id, $event_code, $time, $description, $params = null)
    {
        $params = $params ? $params : [];
        $params['desc'] = $description;

        $new = new EventLog();
        $new->time = Utils::getTimeForDB($time);
        if ($master_class == Department::class) {
            $new->department_id = $master_id;
        } elseif ($master_class == Region::class) {
            $new->region_id = $master_id;
        } else {
            $new->project_id = $master_id;
        }

        $new->user_id = User::getCurrentId();
        $new->event_code = $event_code;
        $new->setParamsArr($params);
        $new->send_time = null;
        $new->show_log = self::getShowCodeByEvent($event_code);
        $new->send_notification = self::getNotificationCodeByEvent($event_code);
        return $new->save();
    }

    public function setParamsArr($array)
    {
        $params = is_null($array) ? null : json_encode($array);
        $this->params = $params;
    }

    public function getParamsArr()
    {
        $params_str = $this->params;
        $array = json_decode($params_str, true);
        if (!$array || !is_array($array)){
            return [];
        } else {
            return $array;
        }
    }

    public function getParam($param)
    {
        $params = $this->getParamsArr();
        if (isset($params[$param])) {
            return $params[$param];
        } else {
            return null;
        }
    }

    public function getDescription()
    {

        $template_code = 'project_log_action_'.$this->event_code;
        return \Yii::t('msg', $template_code, $this->getParamsArr());
    }

    public function getNotificationText()
    {
        $template_code = 'project_log_notification_'.$this->event_code;
        $params = $this->getParamsArr();
        $user = $this->user;
        $params['user'] = $user ? $user->getUserNameLabel() : '?';
        return \Yii::t('msg', $template_code, $params);
    }

    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::class, ['id' => 'department_id']);
    }

    public function getRegion()
    {
        return $this->hasOne(Region::class, ['id' => 'region_id']);
    }

    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public function getTypeLabel()
    {
        switch ($this->event_code) {
            case self::EVENT_MILESTONE: return \Yii::t("app", "Milestone");
            case self::EVENT_MEETING: return  \Yii::t("app", "Meeting");

            case self::ACTION_CHANGE_OBJ_MAIN: return  \Yii::t("app", "Change main objectives");

            case self::ACTION_ADD_OBJ: return  \Yii::t("app", "Add objectives");
            case self::ACTION_CHNG_OBJ: return  \Yii::t("app", "Change objectives");
            case self::ACTION_DEL_OBJ: return  \Yii::t("app", "Delete objectives");

            case self::ACTION_ADD_FOCUS: return  \Yii::t("app", "Add focus objectives");
            case self::ACTION_CHNG_FOCUS: return  \Yii::t("app", "Change focus objectives");
            case self::ACTION_DEL_FOCUS: return  \Yii::t("app", "Delete focus objectives");
        }
        return null;
    }
}