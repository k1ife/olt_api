<?php
namespace common\models\data;

use common\models\data\base\BaseMasterEntity;
use common\models\dict\Role;

class UserRoles
{

    /**
     * @param User $user
     * @param integer $role_code
     * @param BaseMasterEntity $master
     *
     * @return bool
     */
    public static function hasRole($user, $role_code, $master = null)
    {
        $user_roles = static::getUserRoles($user);
        foreach ($user_roles as $user_role) {
            if ($user_role->role_code == $role_code) {
                if ($master) {
                    $user_role_master_info = $user_role->getMasterInfo();
                    if ($user_role_master_info['id'] == $master->id &&
                        is_a($master, $user_role_master_info['class'])) {
                        return true;
                    }
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param User $user
     * @param BaseMasterEntity $master
     *
     * @return UserRole[]
     *@deprecated Only one role may be set to user-master pair
     */
    public static function getUserRolesFor($user, $master)
    {
        $res = [];
        $user_roles = static::getUserRoles($user);
        foreach ($user_roles as $user_role) {
            $ur_master_info = $user_role->getMasterInfo();
            if ($ur_master_info['class'] &&
                is_a($master, $ur_master_info['class']) &&
                $ur_master_info['id'] == $master->id
            ) {
                $res[] = $user_role;
            }
        }
        return $res;
    }

    /**
     * Only ONE role may by set to master
     *
     * @param User $user
     * @param $master
     * @param bool $includeSA
     * @return UserRole|null
     */
    public static function getUserRoleFor($user, $master, $includeSA = false)
    {
        $user_roles = static::getUserRoles($user);
        foreach ($user_roles as $user_role) {
            if ($includeSA && $user_role->role_code === Role::ROLE_ADMIN) {
                return $user_role;
            }
            $ur_master_info = $user_role->getMasterInfo();
            if ($ur_master_info['class'] &&
                is_a($master, $ur_master_info['class']) &&
                $ur_master_info['id'] == $master->id
            ) {
                return $user_role;
            }
        }
        return null;
    }

    public static function hasAnyRoleFor($user, $master)
    {
        $user_roles = static::getUserRoles($user);
        foreach ($user_roles as $user_role) {
            $ur_master_info = $user_role->getMasterInfo();
            if ($ur_master_info['class'] &&
                is_a($master, $ur_master_info['class']) &&
                $ur_master_info['id'] == $master->id
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * get UserRoles of user in role_code desc order
     *
     * @param User $user
     * @return UserRole[]
     */
    public static function getUserRoles($user)
    {
        if (!$user) {
            return [];
        } else {
            return $user->userRoles;
        }
    }

    /**
     * @param $project_id
     * @param null $role_code
     * @return UserRole[]
     */
    public static function findForProject($project_id, $role_code = null)
    {
        $query = UserRole::find()
            ->where(['project_id' => $project_id]);
        if ($role_code) {
            $query->andWhere(['role_code' => $role_code]);
        }
        return $query->all();
    }

}