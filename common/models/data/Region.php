<?php


namespace common\models\data;

use common\models\data\base\BaseMasterEntity;
use common\models\dict\Role;
use common\models\utils\ImageManager;
use common\components\linkerBehavior\LinkerBehavior;
use yii\helpers\ArrayHelper;

/**
 * Class Region
 *
 * @property string $id [integer]
 * @property string $name
 * @property string $image
 * @property bool $is_central [boolean]
 *
 * @property string $caption
 *
 * @property User[] $admins
 * @property integer[] $admins_ids
 */
class Region extends BaseMasterEntity
{
    public function getEntityClass()
    {
        return Region::class;
    }

    static public function getEntityCode()
    {
        return self::ENTITY_REG;
    }

    static public function getEntityUrl()
    {
        return 'regions';
    }


    static public function getEntityLinkField()
    {
        return 'region_id';
    }

    public static function tableName()
    {
        return "regions";
    }

    public function rules()
    {
        return [
            ["id", 'integer'],
            ['name', 'string'],
            ['image', 'string'],

            ["name", "unique"],
            ['is_central', 'boolean'],

            ['admins_ids', 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t("app", "Record name"),
            "is_central" => \Yii::t('app', 'Is Central'),
            'admins_ids' => \Yii::t('app', 'Region admin'),
        ];
    }

    public function behaviors()
    {
        return [
            'manyToMany' => [
                'class' => LinkerBehavior::class,
                'relations' => [
                    'admins_ids' => [
                        'admins',
                        'updater' => [
                            'viaTableAttributesValue' => [
                                'role_code' => Role::ROLE_REG_ADMIN
                            ],
                            'viaTableCondition' => [
                                'role_code' => Role::ROLE_REG_ADMIN
                            ],
                        ]
                    ]
                ],
            ],
        ];
    }

    public function getAdmins()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable(UserRole::tableName(), ['region_id' => 'id'],
            function ($query) {
                $query->andWhere([
                    'role_code' => Role::ROLE_REG_ADMIN,
                ]);
                return $query;
            });
    }

    /**
     * @param User $user
     * @return array [id => name]
     */
    public static function getAllArrForUser($user)
    {
        return ArrayHelper::map(static::getAllForUser($user), "id", "name");
    }

    /**
     * @param User $user
     * @return Department[]
     */
    public static function getAllForUser($user)
    {
        $query = Region::find()
            ->orderBy(['is_central' => SORT_DESC, 'name' => SORT_ASC])
        ;
        if (!$user->isSuperAdmin()) {
            $user_id = $user->id;
            $query
                ->leftJoin(['urr' => 'users_roles'], 'urr.region_id = regions.id')
                ->leftJoin(['pr' => 'projects_regions'], 'regions.id = pr.region_id')
                ->leftJoin(['urp' => 'users_roles'], 'urp.project_id = pr.project_id')
                ->andWhere([
                    'or',
                    ['urr.user_id' => $user_id],
                    ['urp.user_id' => $user_id],
                ]);
        }
        return $query->all();
    }

    public function getImageUrl()
    {
        return ImageManager::getRegionThumb($this);
    }

    /**
     * @param int $page_size
     * @param int $page
     * @return EventLog[]
     */
    public function getLog($page_size = 500, $page = 1)
    {
        $show_log_code = EventLog::SHOW_LOG_PROJECT_PAGE;
        $offset = ($page-1) * $page_size;
        return EventLog::find()
            ->where(['region_id' => $this->id, 'show_log' => $show_log_code])
            ->orderBy(['time' => SORT_DESC])
            ->limit($page_size)
            ->offset($offset)
            ->all()
            ;
    }

    private static $_central_id = null;
    public static function getCentralId() {
        if (static::$_central_id === null) {
            static::$_central_id = Region::find()
                ->select('id')
                ->where(['is_central' => true])
                ->scalar();
        }
        return static::$_central_id;
    }

    public function getCaption()
    {
        return $this->name;
    }



    /**
     * @param User $user
     * @return bool
     */
    public function canView($user)
    {
        return UserPermissions::can($user, UserPermissions::VIEW, $this);
    }

    /**
     * @param User $user
     * @return boolean
     */
    public static function canAdd($user)
    {
        return UserPermissions::can($user, UserPermissions::ADD, Region::class);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canEdit($user)
    {
        return UserPermissions::can($user, UserPermissions::EDIT, $this);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canAddNotes($user)
    {
        return UserPermissions::can($user, UserPermissions::ADD_DETAIL, $this);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canAddEvents($user)
    {
       return UserPermissions::can($user, UserPermissions::ADD_EVENT, $this);
    }

    protected static $_all = null;
    public static function getAll()
    {
        if (!static::$_all) {
            static::$_all = Region::find()->all();
        }
        return static::$_all;
    }

        /**
     * @return array [id] => "name"
     */
    public static function getAllArr()
    {
        $all = static::getAll();
        $res = ArrayHelper::map($all, 'id', 'name');
        return $res;
    }

}