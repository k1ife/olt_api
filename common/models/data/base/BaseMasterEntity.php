<?php
namespace common\models\data\base;

use common\models\data\Department;
use common\models\data\Project;
use common\models\data\Region;
use common\models\data\User;
use common\models\data\UserRoles;
use yii\base\Exception;
use yii\db\ActiveRecord;
use \common\models\data\UserRole;

/**
 * Class BaseMasterEntity
 * @package common\models\data\base
 *
 * @property integer $id
 */
abstract class BaseMasterEntity extends ActiveRecord
{
    public const ENTITY_DEP = 'dep';
    public const ENTITY_REG = 'reg';
    public const ENTITY_PROJ = 'prj';

    abstract public function getEntityClass();
    abstract static public function getEntityCode();
    abstract static public function getEntityUrl();
    abstract static public function getEntityLinkField();

    public function getMyEntityCode()
    {
        $class = static::getEntityClass();
        return $class::getEntityCode();
    }

    public function getMyEntityUrl()
    {
        $class = static::getEntityClass();
        return $class::getEntityUrl();
    }

    public function getMyEntityLinkField()
    {
        $class = static::getEntityClass();
        return $class::getEntityLinkField();
    }

    public static function getEntityClasses()
    {
        return [
            Project::class,
            Department::class,
            Region::class
        ];
    }

    /**
     * @param $entity_code
     * @return string
     * @throws Exception
     */
    public static function getEntityClassByCode($entity_code)
    {
        /** @var BaseMasterEntity $class */
        foreach (static::getEntityClasses() as $class) {
            if ($class::getEntityCode() == $entity_code) {
                return $class;
            }
        }

        throw new Exception("Unknown entity code: $entity_code");
    }

    /**
     * @param $entity_code
     * @param $entity_id
     * @return static|null
     * @throws Exception
     */
    public static function getMasterEntity($entity_code, $entity_id)
    {
        /** @var BaseMasterEntity $class */
        $class = static::getEntityClassByCode($entity_code);
        return $class::findById($entity_id);
    }

    /**
     * @param $id
     * @return static|null
     */
    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByIds($ids)
    {
        return static::find()->where(['id' => $ids])->all();
    }

    /**
     * get max user role
     * @param User $user
     *
     * @return UserRole|null
     */
    public function getMaxUserRole($user)
    {
        return UserRoles::getUserRoleFor($user, $this, true);
    }

    /**
     * @param User $user
     * @return integer|null
     */
    public function getMaxUserRoleCode($user)
    {
        $user_role = $this->getMaxUserRole($user);
        return $user_role ? $user_role->role_code : null;
    }

}