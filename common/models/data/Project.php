<?php

namespace common\models\data;

use common\models\data\base\BaseMasterEntity;
use common\models\dict\ProjectCategory;
use common\models\dict\ProjectType;
use common\models\dict\Role;
use common\models\utils\ITreeObject;
use common\models\utils\Utils;
use common\components\linkerBehavior\LinkerBehavior;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class Project
 * @property integer $id
 * @property integer $type_id
 * @property integer $parent_id
 * @property string $name
 * @property string $description
* // * @property integer $leader_id
 * @property boolean $has_agreement
 * @property string $created_at
 * @property string $closed_at
 * @property integer $positions
 * @property integer $goal_position
 * @property integer $current_position
 * @property string $start_date
 * @property string $close_date
 *
// * @property ProjectType $type
 * @property Project $parent
 * @property Project[] $children
 *
 * @property User[] $leaders
 * @property integer[] $leaders_ids
 * @property ProjectParticipant[] $participants
 * @property ProjectCategory $category
 * @property ProjectRegion[] $projectRegions
 * @property bool $is_para [boolean]
 * @property bool $is_central [boolean]
 * @property Region[] $regions
 * @property Department[] $departments
 * @property integer[] $departments_ids
 *
 * @property integer[] $trainers_ids
 * @property integer[] $sportsmen_ids
 * @property Trainer[] $trainers
 * @property Sportsman[] $sportsmen

 * @property array $regionsModes
 *
 *
 * @property boolean $isClosed
 * @property string $startDateStr
 * @property string $min_note_date [date]
 * @property string $max_note_date [date]
 * @property string $category_id [integer]
 * @property string $goal_label
 * @property string[] $objectives [text[]]
 * @property string[] $focus_area [text[]]
 * @property string $caption
 *
 */
class Project extends BaseMasterEntity implements ITreeObject
{

    public const SCENARIO_EDIT = 'edit';
    public const SCENARIO_SET_GOAL = 'set-goal';

    public function getEntityClass()
    {
        return Project::class;
    }

    static public function getEntityCode()
    {
        return self::ENTITY_PROJ;
    }

    static public function getEntityUrl()
    {
        return 'projects';
    }


    static public function getEntityLinkField()
    {
        return 'project_id';
    }

    public static function tableName()
    {
        return 'projects';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_EDIT] = $scenarios[self::SCENARIO_DEFAULT];
        return $scenarios;
    }


    public function rules()
    {
        $rules = [
            ['id', 'integer'],
            ['type_id', 'integer'],
            ['parent_id', 'integer'],
            ['name', 'string'],
            ['description', 'string'],
//            ['leader_id', 'integer'],
            ['has_agreement', 'boolean'],
            ['created_at', 'safe'],
            ['closed_at', 'safe'],
            [['goal_position', 'current_position'], 'integer'],
            ['positions','integer', 'min' => 15, 'max' => 100, 'on' => 'create'],
            ['category_id', 'integer'],
//            ['departments_ids', 'each', 'rule' => ['integer']],
            ['departments_ids', 'safe'],
            ['is_para', 'boolean'],
            ['is_central', 'boolean'],

//            [['type_id', 'name', 'leader_id'], 'required'],
//            [['type_id', 'name'], 'required'],
//            [['category_id'], 'required'],

//            [['parent_id', 'type_id', 'leader_id'], 'default', 'value' => null],
            [['parent_id', 'type_id', ], 'default', 'value' => null],


            ['isClosed', 'boolean'],
            ['start_date', 'safe'],
            ['startDateStr', 'string'],
            ['close_date', 'safe'],

            ['min_note_date', 'safe'],
            ['max_note_date', 'safe'],
            ['goal_label', 'string', 'on' => self::SCENARIO_SET_GOAL],
            ['objectives', 'safe', 'on' => self::SCENARIO_SET_GOAL], // text[]
            ['focus_area', 'safe', 'on' => self::SCENARIO_SET_GOAL], // text[]

            ['regionsModes', 'safe'],

            ['leaders_ids', 'safe'],

            ['trainers_ids', 'safe'],
            ['sportsmen_ids', 'safe'],
        ];

//        $user = User::curr();
//        if ($user && $user->isLeaderNotAdmin()) {
//            // user is not admin, but leader, so he can set only projects from 2 level
//            $rules[] =
//                ['parent_id', 'required'];
//        }
        return $rules;
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('app', 'Project name'),
            'type_id' => \Yii::t('app', 'Type'),
            'description' => \Yii::t('app', 'Description'),
//            'leader_id' => \Yii::t('app', 'Leader'),
//            'leaders_ids' => \Yii::t('app', 'Project leader'),
            'has_agreement' => \Yii::t('app', 'Has agreement'),
            'parent_id' => \Yii::t('app', 'Parent project'),

            'isClosed' => \Yii::t('app', 'Archived'),

            'positions' => \Yii::t('app', 'Positions'),
            'start_date' => \Yii::t('app', 'Date'),
            'startDateStr' => \Yii::t('app', 'Date'),

            'departments_ids' => \Yii::t('app', 'Department'),
            'category_id' => \Yii::t('app', 'Project category'),

            'regions' => \Yii::t('app', 'Regions'),
            'leaders' => \Yii::t('app', 'Project leader'),
            'participants' => \Yii::t('app', 'Project participants'),
            'guests' => \Yii::t('app', 'Project guests'),

            'trainers' => \Yii::t('app', 'Involved trainers'),
            'trainers_ids' => \Yii::t('app', 'Involved trainers'),
            'sportsmen' => \Yii::t('app', 'Involved sportsmen'),
            'sportsmen_ids' => \Yii::t('app', 'Involved sportsmen'),

            'is_para' => \Yii::t('app', 'Is ParaProject'),
            'is_central' => \Yii::t('app', 'Is Central'),


        ];
    }


    public function behaviors()
    {
        return [
            'manyToMany' => [
                'class' => LinkerBehavior::class,
                'relations' => [
                    'departments_ids' => 'departments',
                    'trainers_ids' => [
                        'trainers',
                        'set' => function($val) {
                            $depId = $this->getDepartmentId();
                            return Trainer::processIdsNames($val, $depId);
                        }
                    ],
                    'sportsmen_ids' => [
                        'sportsmen',
                        'set' => function($val) {
                            $depId = $this->getDepartmentId();
                            return Sportsman::processIdsNames($val, $depId);
                        }
                    ]
//                    'leaders_ids' => [
//                        'leaders',
//                        'updater' => [
//                            'viaTableAttributesValue' => [
//                                'role_code' => Role::ROLE_PROJECT_LEADER
//                            ],
//                            'viaTableCondition' => [
//                                'role_code' => Role::ROLE_PROJECT_LEADER
//                            ],
//                        ]
//                    ],
                ],
            ],
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getParentId()
    {
        return $this->parent_id;
    }


    public function getLeaders()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable(UserRole::tableName(), ['project_id' => 'id'],
            function ($query) {
                $query->andWhere([
                    'role_code' => Role::ROLE_PROJECT_LEADER,
                ]);
                return $query;
            });
    }

    public function getTrainers()
    {
        return $this->hasMany(Trainer::class, ['id' => 'trainer_id'])
            ->viaTable(ProjectTrainer::tableName(), ['project_id' => 'id']);
    }

    public function getSportsmen()
    {
        return $this->hasMany(Sportsman::class, ['id' => 'sportsman_id'])
            ->viaTable(ProjectSportsman::tableName(), ['project_id' => 'id']);
    }

    public function __construct(array $config = [])
    {
        parent::__construct($config);

        $this->start_date = Utils::getTimeForDB();

        $project_positions = isset(\Yii::$app->params['project_positions']) ? \Yii::$app->params['project_positions'] : 30;

        $this->positions = $project_positions;
        $this->current_position = 4; // init man to position 4
        $this->goal_position = 1;   // init goal to position 1

        $user = User::curr();
        if ($user && !$user->isSuperAdmin()) {
            // for leader - set defaults
//            $this->leaders_ids = [$user->id];

        }
    }

    public function getDepartments()
    {
        return $this->hasMany(Department::class, ['id' => 'department_id'])
            ->viaTable('projects_departments', ['project_id' => 'id']);
    }

    public function getType()
    {
        return $this->hasOne(ProjectType::class, ['id' => 'type_id']);
    }

    public function getParent()
    {
        return $this->hasOne(Project::class, ['id' => 'parent_id']);
    }

    public function getChildren()
    {
        return $this->hasMany(Project::class, ['parent_id' => 'id']);
    }

    public function isOpen()
    {
        return $this->closed_at == null;
    }

    public function getParticipants()
    {
        return $this->hasMany(ProjectParticipant::class, ['project_id' => 'id']);
    }

    public function getCategory()
    {
        return $this->hasOne(ProjectCategory::class, ['id' => 'category_id']);
    }

    /**
     * Prepared possible parent projects for user and current project
     * @param User $user
     * @param Project|null $project
     * @return Project[]
     */
    public static function getAllAsParentsForUserAndProject(User $user, Project $project = null)
    {
        $query = Project::find();
        $and_where = [];

        $query->andWhere(['closed_at'=> null]);

        if ($project && $project->id){
            // exclude project with its children
            $exclude_ids = $project->getAllBranchIds(true);
            if (!empty($exclude_ids)){
                $and_where[] = ['not in', 'id', $exclude_ids];
            }

        }
        if (!$user->isSuperAdmin()) {
            // get user departments
            $departmentsArr = Department::getAllArrForUser($user);
            $dep_ids = array_keys($departmentsArr);
            if (!empty($dep_ids)) {
                $query->leftJoin('projects_departments', 'projects.id = projects_departments.project_id');
                $and_where[] = ['department_id' => $dep_ids];
            } else {
//                $and_where[] = ['leader_id' => $user->id];
            }
        }

        if (count ($and_where)) {
            array_unshift($and_where, 'and');

            if ($project && $project->parent_id) {
                $query->where(['or', $and_where, ['id' => $project->parent_id]]);
            } else {
                $query->where($and_where);
            }
        } else {
            if ($project && $project->parent_id) {
                $query->where(['id' => $project->parent_id]);
            }
        }

        $all = $query->all();
        return $all;
    }

    public function beforeSave($insert)
    {
        if (is_null($this->created_at)){
            $this->created_at = Utils::getTimeForDB();
        }

        // set attributes from parent
        if ($this->parent_id) {
            $parent = $this->parent;
            $this->category_id = $parent->category_id;
            $this->departments_ids = $parent->departments_ids;
        }


        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        

        if ($insert){
            EventLog::addEvent($this, EventLog::ACTION_CREATE_PROJECT, [
                'name' => $this->name
            ]);
//            if ($this->leaders_ids) {
//                EventLog::addEvent($this, EventLog::ACTION_SET_LEADER, [
//                    'project' => $this->name,
//                    'project_id' => $this->id
//                ]);
//            }
        } else {
            if (array_key_exists('current_position', $changedAttributes)){
                EventLog::addEvent($this, EventLog::ACTION_CHANGE_CURRENT_POSITION, [
                    'old_position' => $changedAttributes['current_position'],
                    'new_position' => $this->current_position
                ]);
                unset ($changedAttributes['current_position']);
            }
            if (array_key_exists('goal_position', $changedAttributes)){
                EventLog::addEvent($this, EventLog::ACTION_CHANGE_GOAL_POSITION, [
                    'old_position' => $changedAttributes['goal_position'],
                    'new_position' => $this->goal_position
                ]);
                unset ($changedAttributes['goal_position']);
            }

            if (array_key_exists('name', $changedAttributes)) {
                EventLog::addEvent($this, EventLog::ACTION_EDIT_PROJECT, [
                    'name' => $this->name,
                    'old_name' => $changedAttributes['name']
                ]);
            }
            if (array_key_exists('leaders_ids', $changedAttributes)) {
                EventLog::addEvent($this, EventLog::ACTION_SET_LEADER, [
                    'project' => $this->name,
                    'project_id' => $this->id
                ]);
            }

            if (array_key_exists('goal_label', $changedAttributes)) {
                EventLog::addEvent($this, EventLog::ACTION_CHANGE_OBJ_MAIN, [
                    'project' => $this->name,
                    'project_id' => $this->id,
                    'value' => $this->goal_label,
                    'old_value' => $changedAttributes['goal_label']
                ]);
            }
            if (array_key_exists('objectives', $changedAttributes)) {
                // get changed item
                // we need to count arrays diffs:
                // $this->objectives (new arr) and $changedAttributes->value (old arr)
                // but it's has some difficults, so add this events int controller
                // where we have all need information
            }

            // change category and department for all children
            $need_correct_children = false;

            $need_correct_children = true;
//            if (array_key_exists('department_id', $changedAttributes)) {
//                $need_correct_children = true;
//                $old_dep = Department::findById($changedAttributes['department_id']);
//                EventLog::addEvent($this, EventLog::ACTION_CHANGE_PROJECT_DEPARTMENT, [
//                    'old_department_name' => $old_dep ? $old_dep->name : '-',
//                    'new_department_name' => $this->department->name,
//                    'project_id' => $this->id
//                ]);
//            }
//            if (array_key_exists('category_id', $changedAttributes)) {
//                $need_correct_children = true;
//                $old_cat = ProjectCategory::findById($changedAttributes['category_id']);
//                EventLog::addEvent($this, EventLog::ACTION_CHANGE_PROJECT_CATEGORY, [
//                    'old_category_name' => $old_cat ? $old_cat->name : '-',
//                    'new_category_name' => $this->category->name,
//                    'project_id' => $this->id
//                ]);
//            }

            if ($need_correct_children) {
                $children = $this->children;
                if (count($children)) {
                    foreach ($children as $child) {
                        $child->departments_ids = $this->departments_ids;
                        $child->category_id = $this->category_id;
                        $child->save();
                    }
                }
            }
        }

        if ($this->is_central) {
            $this->regionsModes = [Region::getCentralId() => ProjectRegion::MODE_NORMAL];
        }

        if ($this->scenario === self::SCENARIO_EDIT && is_null($this->_region_modes)) {
            // not set regionModes - course post query does not have any values selected
            // force set by empty array
            $this->regionsModes = [];
        }
        if ($this->_region_modes_changed) {
            $this->saveRegions();
        }

//        // set project leader if project is new and has no leaders
//        if ($insert) {
//            if (count($this->leaders) == 0) {
//                $leaderRole = new UserRole();
//                $leaderRole->project_id = $this->id;
//                $leaderRole->role_code = Role::ROLE_PROJECT_LEADER;
//                $leaderRole->user_id = User::getCurrentId();
//                $leaderRole->save();
//            }
//        }


    }

    protected function saveRegions()
    {
        // save current regions
        ProjectRegion::updateForProject($this->id, $this->regionsModes);
        $this->correctParentRegions();
    }


    protected function getAllBranchIds($include_self_id = true)
    {
        $res = [];
        if ($include_self_id){
            $res[] = $this->id;
        }

        $curr_level_ids = [$this->id];
        do {
            $query = new Query();
            $query->select(['id'])
                ->from(Project::tableName())
                ->where(['parent_id' => $curr_level_ids]);
            $curr_level_ids = $query->column();
            if (!empty($curr_level_ids)){
                $res = array_merge($res, $curr_level_ids);
            }
        } while (!empty($curr_level_ids));
        return $res;
    }


    /**
     * @param $id
     * @return static|null
     */
    public static function findById($id)
    {
        return static::findOne($id);
    }

    public function setCurrentPosition($position)
    {
        $this->current_position = $position;
        if (!$this->save(false)){
            $err_str = Utils::implode('; ', $this->errors);
            \Yii::error('setCurrentPosition: error save project: '.$err_str);
        }
    }

    public function setGoalPosition($position)
    {
        $this->goal_position = $position;
        if (!$this->save(false)){
            $err_str = Utils::implode('; ', $this->errors);
            \Yii::error('setGoalPosition: error save project: '.$err_str);
        }
    }

    /**
     * @param int $page_size
     * @param int $page
     * @return EventLog[]
     */
    public function getLog($page_size = 500, $page = 1)
    {
        $show_log_code = EventLog::SHOW_LOG_PROJECT_PAGE;
        $offset = ($page-1) * $page_size;
        return EventLog::find()
            ->where(['project_id' => $this->id, 'show_log' => $show_log_code])
            ->orderBy(['time' => SORT_DESC])
            ->limit($page_size)
            ->offset($offset)
            ->all()
            ;
    }


    /**
     * @param int $page_size
     * @param int $page
     * @return EventLog[]
     */
    public function getMilestones($page_size = 500, $page = 1)
    {
        $offset = ($page-1) * $page_size;
        return EventLog::find()
            ->where(['project_id' => $this->id, 'event_code' => [
                EventLog::EVENT_MILESTONE,
                EventLog::EVENT_MEETING
            ]])
            ->orderBy(['time' => SORT_DESC])
            ->limit($page_size)
            ->offset($offset)
            ->all()
            ;
    }

    /**
     * @return EventLog[]
     */
    public function getPositionsLog()
    {
        return EventLog::find()
            ->where(['project_id' => $this->id])
            ->andWhere(['event_code' => [
                EventLog::ACTION_CREATE_PROJECT,
                EventLog::ACTION_CHANGE_CURRENT_POSITION,
                EventLog::ACTION_CHANGE_GOAL_POSITION,

                EventLog::ACTION_CHANGE_OBJ_MAIN,
                EventLog::ACTION_ADD_OBJ,
                EventLog::ACTION_DEL_OBJ,
                EventLog::ACTION_CHNG_OBJ,
                EventLog::ACTION_ADD_FOCUS,
                EventLog::ACTION_DEL_FOCUS,
                EventLog::ACTION_CHNG_FOCUS,

            ]])
            ->orderBy(['time' => SORT_ASC])
            ->all()
            ;
    }

//    public function canManageHeadings(User $user)
//    {
//        if ($this->isClosed){
//            return false;
//        }
//        return $this->isUserAdmin($user) || $this->isUserLeader($user);
//    }
//
//    public function getActiveHeadings()
//    {
//        return $this->hasMany(Heading::className(), ['project_id' => 'id'])
//            ->andWhere(['deleted_at' => null]);
//    }
//
//    /**
//     * @return array    ['positive' => Heading[], 'negative' => Heading[]]
//     */
//    public function getActiveHeadingsByGroups()
//    {
//        $headings = $this->activeHeadings;
//        $positive_headings = [];
//        $negative_headings = [];
//        foreach ($headings as $heading){
//            if ($heading->isPositive()){
//                $positive_headings[] = $heading;
//            } else {
//                $negative_headings[] = $heading;
//            }
//        }
//        return [
//            'positive' => $positive_headings,
//            'negative' => $negative_headings
//        ];
//    }
//
//    public function getAllHeadings()
//    {
//        return $this->hasMany(Heading::className(), ['project_id' => 'id'])
//            ->orderBy([new Expression('CASE WHEN deleted_at IS NULL THEN 0 ELSE 1 END ASC'), 'name' => SORT_ASC]);
//    }
//
//    public static function findHeadings($project_id, $active = true)
//    {
//        $query = Heading::find()
//            ->where(['project_id' => $project_id]);
//        if ($active === true){
//            $query->andWhere(['deleted_at' => null]);
//        } elseif ($active === false){
//            $query->andWhere(['not', ['deleted_at' => null]]);
//        }
//
//        $query->orderBy(['is_positive' => SORT_DESC, 'id' => SORT_ASC]);
//
//        return $query->all();
//    }
//
//
//
//    public function getDetails($user_ids = null, $heading_ids = null,
//                            $time_from = null, $time_to = null,
//                            $only_active_headings = true)
//    {
//        $query = Detail::find()
//            ->innerJoinWith(['heading'])
//            ->where(['project_id' => $this->id])
//            ->orderBy([Detail::tableName().'.date' => SORT_ASC]);
//
//        if ($only_active_headings) {
//            $query->andWhere([Heading::tableName().'.deleted_at' => null]);
//        }
//
//        if ($user_ids) {
//            $query->andWhere(['created_by' => $user_ids]);
//        }
//
//        if ($heading_ids) {
//            $query->andWhere(['heading_id' => $heading_ids]);
//        }
//
//        if ($time_from) {
//            $query->andWhere(['>=', Detail::tableName().'.date', Utils::getTimeForDB($time_from)]);
//        }
//
//        if ($time_to) {
//            $query->andWhere(['<=', Detail::tableName().'.date', Utils::getTimeForDB($time_to)]);
//        }
//
//
//        return $query->all();
//    }


    public function getIsClosed()
    {
        return $this->close_date !== null;
    }

    public function setIsClosed($is_closed)
    {
        if ($is_closed && !$this->closed_at){
            $this->closed_at = Utils::getTimeForDB();
            $this->close_date = Utils::getTimeForDB();

        } elseif (!$is_closed && $this->closed_at){
            $this->closed_at = null;
            $this->close_date = null;
        }
    }

    public function getStartDateStr()
    {
        return $this->getStartDateToShow()->format('Y-m-d');
    }
    public function setStartDateStr($date_str)
    {
        $this->start_date = $date_str;
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {

        if ($this->parent_id) {
            $parent = $this->parent;
            if ($this->category_id === null) {
                $this->category_id = $parent->category_id;
            }
//            if ($this->department_id === null) {
//                $this->department_id = $parent->department_id;
//            }
            // note: in beforeSave method category and department will set to parents values
            if ($this->category_id !== null && $this->category_id != $parent->category_id) {
                $this->addError("category_id", \Yii::t('app', "Nested project must have the same category as parent"));
            }
//            if ($this->department_id !== null && $this->department_id != $parent->department_id) {
//                $this->addError("department_id", \Yii::t('app', "Nested project must have the same department as parent"));
//            }
        }


        parent::validate($attributeNames, $clearErrors);

        $detail_date = Utils::time($this->min_note_date);
        $start_date = Utils::time($this->start_date);
        if ($detail_date && $start_date && $detail_date < $start_date) {
            $this->addError('start_date', \Yii::t('app', 'Wrong date. Project contains some details with earler dates'));
            $this->addError('startDateStr', \Yii::t('app', 'Wrong date. Project contains some details with earler dates'));
        }

        return !$this->hasErrors();
    }

//    public function correctDetailsDateRage()
//    {
//        $HT = Heading::tableName();
//        $query = Detail::find()
//            ->select([
//                'min_date' => new Expression('MIN(date)'),
//                'max_date' => new Expression('MAX(date)'),
//                ])
//            ->innerJoin($HT, "$HT.id = heading_id")
//            ->asArray()
//            ->where(['project_id' => $this->id])
//            ->andWhere(["$HT.deleted_at" => null])
//            ;
//        $min_max = $query->one();
//        if ($min_max) {
//            $min_date_str = $min_max['min_date'];
//            $max_date_str = $min_max['max_date'];
//            $this->min_details_date = Utils::getTimeForDB(Utils::time($min_date_str));
//            $this->max_details_date = Utils::getTimeForDB(Utils::time($max_date_str));
//        } else {
//            $this->min_details_date = null;
//            $this->max_details_date = null;
//        }
//        $this->save(false, ['min_details_date', 'max_details_date']);
//    }

    /**
     * @return \DateTime
     */
    public function getStartDateToShow()
    {
        $min_notes_date = Utils::time($this->min_note_date);
        $start_date = Utils::time($this->start_date);
        $min_date = $min_notes_date ? min ($start_date, $min_notes_date) : $start_date;
        return $min_date;
    }

    /**
     * @return \DateTime|null
     */
    public function getCloseDateToShow()
    {
        $max_notes_date = Utils::time($this->max_note_date);
        $close_date = Utils::time($this->close_date);
        $current_date = Utils::getNow();
        if ($close_date) {
            $max_date = $max_notes_date ? max ($max_notes_date, $close_date) : $close_date;
        } else {
//            $max_date = $max_details_date ? max ($max_details_date, $current_date) : $current_date;
            $max_date = null;
        }
        return $max_date;
    }

    /**
     * @return bool
     */
    public function hasParent()
    {
        return $this->parent_id != null;
    }

    public function getProjectRegions()
    {
        return $this->hasMany(ProjectRegion::class, ['project_id' => 'id']);
    }

    public function getRegions()
    {
        return $this->hasMany(Region::class, ['id' => 'region_id'])
            ->via('projectRegions');
    }

    public function getRegionsIds()
    {
        return ProjectRegion::find()
            ->select(['region_id'])
            ->where(['project_id' => $this->id])
            ->column();
    }

    private function getChildrenRegionIds()
    {
        $children_regions_ids = [];
        $children = $this->children;
        foreach ($children as $child) {
            $child_regions_ids = array_keys($child->regionsModes);
            $children_regions_ids = array_merge($children_regions_ids, $child_regions_ids);
        }
        $children_regions_ids = array_unique($children_regions_ids, SORT_NUMERIC);
        return $children_regions_ids;
    }


    /**
     * @return array [region_id => region_mode]
     */
    private $_region_modes = null;
    private $_region_modes_changed = false;
    public function getRegionsModes()
    {
        if (is_null($this->_region_modes)) {
            $regions = $this->projectRegions;
            $res = ArrayHelper::map($regions, "region_id", "mode");
            $this->_region_modes = $res;
        }
        return $this->_region_modes;
    }

    public function setRegionsModes($new_modes)
    {
        $old_modes = $this->regionsModes;
        // first convert form values to constants and non normal modes
        foreach ($new_modes as $region_id => $mode) {
            if ($mode == 1) {
                $new_modes[$region_id] = ProjectRegion::MODE_NORMAL;
            } else {
                unset($new_modes[$region_id]);
            }
        }
        // check children regions
        $children_regions_ids = $this->getChildrenRegionIds();
        foreach ($children_regions_ids as $child_region_id) {
            if (!isset($new_modes[$child_region_id])) {
                $new_modes[$child_region_id] = ProjectRegion::MODE_CHILDREN;
            }
        }

        // now $new_modes has correct data
        $is_different = count($old_modes) != count($new_modes);
        if (!$is_different) {
            $is_different = count(array_diff_assoc($old_modes, $new_modes));
        }
        if ($is_different) {
            // if  old and new are different
            $this->_region_modes = $new_modes;
            $this->_region_modes_changed = true;
        }

    }

    /**
     * when change project regions, we need check regions of parents:
     * 1. parent must have all children regions
     * 2. if some regions was add to parent only as children regions (MODE_CHILDREN), and children not already
     *    have such region - delete it from parent
     */
    private function correctParentRegions()
    {
        $parent = $this->parent;
        if ($parent) {
            $children_reg_ids = $parent->getChildrenRegionIds();
            $parent_reg_modes = $parent->regionsModes;
            $new_reg_modes = [];
            foreach ($children_reg_ids as $id) {
                $new_reg_modes[$id] = ProjectRegion::MODE_CHILDREN;
            }
            foreach ($parent_reg_modes as $id => $mode) {
                if ($mode == ProjectRegion::MODE_NORMAL) {
                    $new_reg_modes[$id] = ProjectRegion::MODE_NORMAL;
                }
            }
            $parent->regionsModes = $new_reg_modes;
            $parent->save();
        }
    }

    public function getObjectivesArr()
    {
        $objectives = $this->objectives;
        if (!$objectives) {
            return [];
        } else {
            return $objectives->getValue();
        }
    }

    public function getFocusAreaArr()
    {
        $focus = $this->focus_area;
        if (!$focus) {
            return [];
        } else {
            return $focus->getValue();
        }
    }

    public function setObjectivesArr($arr)
    {
        $this->objectives = Utils::fillArr($arr);
    }

    public function setFocusAreaArr($arr)
    {
        $this->focus_area = Utils::fillArr($arr);
    }

    public function getCaption()
    {
        return $this->name;
    }

    public function canEdit(User $user = null)
    {
        return UserPermissions::can($user, UserPermissions::EDIT, $this);
    }

    public function canClose(User $user = null)
    {
        return UserPermissions::can($user, UserPermissions::DELETE, $this);
    }

    public function canView(User $user = null)
    {
        return UserPermissions::can($user, UserPermissions::VIEW, $this);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canAddNotes($user)
    {
        return UserPermissions::can($user, UserPermissions::ADD_DETAIL, $this);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canAddEvents($user)
    {
       return UserPermissions::can($user, UserPermissions::ADD_EVENT, $this);
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canEditGoals($user = null)
    {
        return UserPermissions::can($user, UserPermissions::EDIT_GOALS, $this);
    }

    public function canEditPositions(User $user)
    {
        return UserPermissions::can($user, UserPermissions::EDIT_POSITIONS, $this);
    }

    public function canManage(User $user = null)
    {
        return UserPermissions::can($user, UserPermissions::MANAGE, $this);
    }

    /**
     * Now only ONE department can be set to project
     *
     * @return Department
     */
//    public function getDepartment()
//    {
//        $departments = $this->departments;
//        if (count($departments)) {
//            return $departments[0];
//        }
//    }
//


    /**
     * @return int|null
     */
    public function getDepartmentId()
    {
        $department_ids = $this->departments_ids;
        if ($department_ids && !is_array($department_ids)) {
            return $department_ids;
        } elseif ($department_ids && is_array($department_ids) && count($department_ids)) {
            return $department_ids[0];
        } else {
            return null;
        }
    }
}