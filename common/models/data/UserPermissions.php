<?php
namespace common\models\data;

use common\models\data\details\Detail;
use common\models\dict\Role;

class UserPermissions
{
    const VIEW = 'view';
    const ADD = 'add';
    const EDIT = 'edit';
    const DELETE = 'delete';
    const MANAGE = 'manage';
    const ADD_DETAIL = 'add_detail';
    const ADD_PROJECT = 'add_project';
    const ADD_EVENT = 'add_event';
    const EDIT_GOALS = 'edit_goals';
    const EDIT_POSITIONS = 'edit_positions';

    /**
     * @param User $user
     * @param string $action
     * @param $subject
     */
    public static function can ($user, $action, $subject)
    {
        $user = User::curr($user);

        if (self::isUserSuperAdmin($user)) {
            // sa can do anything
            return true;
        }

        if ($subject == Project::class || is_a($subject, Project::class)) {
            return static::canProject($user, $action, $subject);

        } elseif ($subject == Region::class || is_a($subject, Region::class)) {
            return static::canRegion($user, $action, $subject);

        } elseif ($subject == Department::class || is_a($subject, Department::class)) {
            return static::canDepartment($user, $action, $subject);

        } elseif ($subject == Detail::class || is_a($subject, Detail::class)) {
            return static::canDetail($user, $action, $subject);
        }

        return false;
    }

    /**
     * @param User $user
     * @return mixed
     */
    private static function isUserSuperAdmin($user)
    {
        return $user ? $user->isSuperAdmin() : false;
    }

    /**
     * @param User $user
     * @param $action
     * @param Project $project
     */
    private static function canProject($user, $action, $project)
    {
        switch ($action) {
            case self::ADD:
                // Only sa can add new project
                // Also check ADD_PROJECT for departments
                return false;
                break;

            case self::DELETE:
                return false;
            case self::EDIT:
            case self::EDIT_GOALS:
            case self::EDIT_POSITIONS:
                if ($project->isClosed) {
                    return false;
                }
                $can = UserRoles::hasRole($user, Role::ROLE_PROJECT_LEADER, $project);
                if (!$can) {
                    $can =  self::isUserDepAdminOfProject($user, $project) ||
                            self::isUserRegAdminOfProject($user, $project);
                }
                return $can;
                break;

            case self::VIEW:
                return  UserRoles::hasAnyRoleFor($user, $project) ||
                        self::canProject($user, self::EDIT, $project);
                break;
            case self::ADD_DETAIL:
                if ($project->isClosed) {
                    return false;
                }
                return  UserRoles::hasAnyRoleFor($user, $project);
                break;
            case self::ADD_EVENT:
                if ($project->isClosed) {
                    return false;
                }
                return  UserRoles::hasRole($user, Role::ROLE_PROJECT_LEADER, $project) ||
                        self::isUserDepAdminOfProject($user, $project);
                break;

        }
        return false;
    }

    private static function isUserDepAdminOfProject($user, Project $project)
    {
        $departments = $project->departments;
        if (count($departments)) {
            foreach ($departments as $department) {
                if (UserRoles::hasRole($user, Role::ROLE_DEP_ADMIN, $department)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static function isUserRegAdminOfProject($user, Project $project)
    {
        $regions = $project->regions;
        if (count($regions)) {
            foreach ($regions as $region) {
                if (UserRoles::hasRole($user, Role::ROLE_REG_ADMIN, $region)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param User $user
     * @param $action
     * @param Region $region
     */
    private static function canRegion($user, $action, $region)
    {
        switch ($action) {
            case self::VIEW:
                // can view any user of region or linked project (with any role)
                $regs_arr = Region::getAllArrForUser($user);
                return array_key_exists($region->id, $regs_arr);
                break;
            case self::ADD:
                break;
            case self::EDIT:
            case self::ADD_DETAIL:
            case self::ADD_EVENT:
            case self::ADD_PROJECT:
                return UserRoles::hasRole($user, Role::ROLE_REG_ADMIN, $region);
                break;
        }
        return false;
    }

    /**
     * @param User $user
     * @param $action
     * @param Department $department
     */
    private static function canDepartment($user, $action, $department)
    {
        switch ($action) {
            case self::VIEW:
                // can view any user of region or linked project (with any role)
                $deps_arr = Department::getAllArrForUser($user);
                return array_key_exists($department->id, $deps_arr);
                break;
            case self::ADD:
                break;
            case self::EDIT:
            case self::DELETE:
                // only sa can edit dep params
                break;
            case self::ADD_DETAIL:
            case self::ADD_EVENT:
            case self::ADD_PROJECT:
                return UserRoles::hasRole($user, Role::ROLE_DEP_ADMIN, $department);
                break;
        }
        return false;
    }

    /**
     * @param User $user
     * @param $action
     * @param Detail $detail
     * @return bool|void
     */
    private static function canDetail($user, $action, $detail)
    {
        switch ($action) {
            case self::VIEW:
                return true;
            case self::EDIT:
            case self::DELETE:
                return $detail->created_by == $user->id;
        }
        return false;
    }
}