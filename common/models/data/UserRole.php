<?php

namespace common\models\data;

use common\models\dict\Role;
use common\models\dict\Subrole;
use common\models\notifications\Notificator;
use yii\db\ActiveRecord;

/**
 * Class UserRole
 * @package common\models\data
 *
 * @property integer $id [integer]
 * @property integer $user_id [integer]
 * @property integer $role_code [integer]
 * @property integer $subrole_id [integer]
 * @property integer $region_id [integer]
 * @property integer $department_id [integer]
 * @property integer $project_id [integer]
 *
 * @property User $user
 * @property Role $role
 * @property Subrole $subrole
 * @property Department $department
 * @property Region $region
 * @property Project $project
 *
 * @property Department|Region|Project $master
 */
class UserRole extends ActiveRecord
{

    public static function tableName()
    {
        return 'users_roles';
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['user_id', 'integer'],
            ['role_code', 'integer'],
            ['subrole_id', 'integer'],
            ['region_id', 'integer'],
            ['department_id', 'integer'],
            ['project_id', 'integer'],

            ['role_code', 'unique', 'targetAttribute' => [
                'user_id',
                'role_code',
                'subrole_id',
                'region_id', 'department_id', 'project_id'
            ],
                'message' => \Yii::t('app', 'Already exists')]

        ];
    }

    public function load($data, $formName = null)
    {
        // Обязательно перед загрузкой с формы - очистим поля,
        // потому что может остаться мусор в скрытых полях
        $this->subrole_id = null;
        $this->region_id = null;
        $this->department_id = null;
        $this->project_id = null;

        return parent::load($data, $formName);
    }


    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return Role
     */
    public function getRole()
    {
        return Role::getByCode($this->role_code);
    }

    public function getSubrole()
    {
        return $this->hasOne(Subrole::class, ['id' => 'subrole_id']);
    }

    public function getRegion()
    {
        return $this->hasOne(Region::class, ['id' => 'region_id']);
    }

    public function getDepartment()
    {
        return $this->hasOne(Department::class, ['id' => 'department_id']);
    }

    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    public function save($runValidation = true, $attributeNames = null)
    {
        $check = $this->checkBeforeUpdate();
        if ($check == true) {
            try {
                return parent::save($runValidation, $attributeNames);
            } catch (\Exception $e) {
                $this->addError('', 'System error. ' . $e->getMessage());

                return false;
            }
        } elseif ($check === null) {
            // current role not save, but was changes for found another role
            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // check project leader
        if ($this->role_code == Role::ROLE_PROJECT_LEADER) {

            $users_roles = UserRoles::findForProject($this->project_id, Role::ROLE_PROJECT_LEADER);
            foreach ($users_roles as $user_role) {
                if ($user_role->id != $this->id) {
                    // another leader - change to participant
                    $user_role->role_code = Role::ROLE_PROJECT_PARTICIPANT;
                    $user_role->save();
                }
            }
        }

        // send change project role notification
        $master_info = $this->getMasterInfo();
        if ($master_info['class'] == Project::class) {
            if (array_key_exists('role_code', $changedAttributes)) {
                // project role was changed
                $old_role_code = $changedAttributes['role_code'];
                if ($old_role_code) {
                    // only if old role exists (role changing - not new assigning)
                    $new_role_code = $this->role_code;
                    Notificator::onChangeProjectRole(
                        $this->user,
                        $this->project,
                        Role::getByCode($old_role_code),
                        Role::getByCode($new_role_code)
                    );
                }
            }
        }

    }

    /**
     * true - let's make change
     * false - error
     * null - no error, no action - in backside old role was chaged
     * @return bool|null
     */
    protected function checkBeforeUpdate()
    {
        $old_role = UserRoles::getUserRoleFor($this->user, $this->master);

        if ($old_role && $old_role->id != $this->id) {
            // if user already has some role in this master
            $master_info = $this->getMasterInfo();
            if ($master_info['class'] == Project::class) {
                // some logic for projects

                $old_role_code = $old_role->role_code;
                $new_role_code = $this->role_code;
                if ($old_role_code > $new_role_code) {
                    $this->addError("role", \Yii::t('app', 'User already in project'));
                    return false;
                } else {
                    // change old role
                    $old_role->role_code = $this->role_code;
                    $old_role->subrole_id = $this->subrole_id;
                    $old_role->save();
                    return null;
                }

            } else {

                $name = $old_role->getMasterCaption();
                $this->addError('role', \Yii::t('app', 'User already has role in "{0}"', $name));
                return false;
            }

        } else {
            return true;
        }
    }

    /**
     * @return array ['class' => string, 'id' => integer]
     */
    public function getMasterInfo()
    {

        if ($this->region_id) {
            return [
                'class' => Region::class,
                'id' => $this->region_id
            ];
        } elseif ($this->department_id) {
            return [
                'class' => Department::class,
                'id' => $this->department_id
            ];
        } elseif ($this->project_id) {
            return [
                'class' => Project::class,
                'id' => $this->project_id
            ];
        } else {
            return [
                'class' => null,
                'id' => null
            ];
        }
    }

    /**
     * @return Region|Department|Project|null
     */
    public function getMaster()
    {
        $master_info = $this->getMasterInfo();
        $class = $master_info['class'];
        $id = $master_info['id'];
        if (!$class) {
            return null;
        } else {
            return $class::findById($id);
        }
    }

    public function getMasterCaption()
    {
        $master = $this->getMaster();
        return $master ? $master->caption : "System";
    }

}