<?php


namespace common\models\dict;

use common\models\data\User;
use common\models\data\UserRole;

/**
 * Class Subrole
 * @package common\models\dict
 *
 * @property string $id [integer]
 * @property string $role [integer]
 * @property string $name
 */

class Subrole extends BaseDict
{
    public static function tableName()
    {
        return "subroles";
    }

    public function rules()
    {
        return [
            ["id", 'integer'],
            ["name", "string"],
            ["role", "integer"],

            ["name", "unique"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t("app", "Record name")
        ];
    }


    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->role = Role::ROLE_PROJECT_PARTICIPANT;
    }

    static private $_all;
    static protected function getAllCached()
    {
        return static::$_all;
    }

    static protected function setAllCached($all)
    {
        static::$_all = $all;
    }

}