<?php

namespace common\models\dict;


/**
 * Class ProjectCategory
 * @package common\models\dict
 *
 * @property string $id [integer]
 * @property string $name
 */
class ProjectCategory extends BaseDict
{
    public static function tableName()
    {
        return "project_categories";
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['name', 'string'],

            ["name", "unique"],
        ];
    }

    static private $_all;
    static protected function getAllCached()
    {
        return static::$_all;
    }

    static protected function setAllCached($all)
    {
        static::$_all = $all;
    }

}