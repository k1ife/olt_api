<?php
namespace common\models\dict;

use common\models\data\base\BaseMasterEntity;
use common\models\data\Department;
use common\models\data\details\Detail;
use common\models\data\Project;
use common\models\data\Region;
use common\models\detail_formats\DetailFormat;
use common\models\utils\Utils;

/**
 * Class DetailCategory
 * @package common\models\dict
 *
 * @property integer $id [integer]
 * @property string $name
 * @property string $color
 *
 * @property boolean $prj
 * @property boolean $dep
 * @property boolean $reg
 * @property string $delete_date [date]
 * @property integer $sort
 *
 * @property bool $is_internal [boolean]
 * @property bool $is_project_manage [boolean]
 *
 */
class DetailCategory extends BaseDict
{
    public static function tableName()
    {
        return 'detail_categories';
    }

    public function rules()
    {
        return [
            ["id", "integer"],
            ["name", "string"],
            ["color", "string"],
            ['delete_date', 'safe'],
            ['is_internal', 'safe'],
            ['is_project_manage', 'safe'],
            ['sort', 'integer'],

//            ["name", "unique"],
            [[
                Project::getEntityCode(),
                Department::getEntityCode(),
                Region::getEntityCode()], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('app', 'Name'),
            'color' => \Yii::t('app', 'Color'),
            'is_internal' => \Yii::t('app', 'Internal'),
            'is_project_manage' => \Yii::t('app', 'Project manage')
        ];
    }

    protected static function getSortField()
    {
        return 'sort';
    }

    public static function find()
    {
        return parent::find()
            ->orderBy(['sort' => SORT_ASC]);
    }


    static private $_all;

    static protected function getAllCached()
    {
        return static::$_all;
    }

    static protected function setAllCached($all)
    {
        static::$_all = $all;
    }

    static public function getAllForEntityClass(BaseMasterEntity $entity, $showDeleted, $showInternal = false)
    {
        $all = static::getAll();
        $res = [];
        $entity_class = $entity->getEntityClass();
        $entity_code = $entity_class::getEntityCode();

        foreach ($all as $item) {
            if ($item->$entity_code &&
                ($showDeleted || !$item->isDeleted()) &&
                ($showInternal || !$item->isInternal())
            ) {
                $res[] = $item;
            }
        }

        return $res;
    }

    public function countDetails()
    {
        return Detail::find()
            ->where(['category_id' => $this->id])
            ->count();
    }

    public function countProjects()
    {
        $proj_ids = Detail::find()
            ->where(['category_id' => $this->id])
            ->select('project_id')
            ->distinct()
            ->column();
        return count($proj_ids);
    }

    public function countDepartments()
    {
        $deps_ids = Detail::find()
            ->where(['category_id' => $this->id])
            ->select('department_id')
            ->distinct()
            ->column();
        return count($deps_ids);
    }

    public function delete()
    {
        $countDetails = $this->countDetails();
        if ($countDetails > 0) {
            $this->delete_date = Utils::getTimeForDB();
            return $this->save(false, ['delete_date']);
        } else {
            return parent::delete();
        }
    }

    public function fullDelete()
    {
        Detail::deleteAll(['category_id' => $this->id]);
        return parent::delete();
    }

    public function isDeleted()
    {
        return $this->delete_date != null;
    }

    public function isInternal()
    {
        return $this->is_internal;
    }

    public function restore()
    {
        $this->delete_date = null;
        return $this->save(false, ['delete_date']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        // add default detail formats to new category
        $addToFormats = [];

        $allFormats = DetailFormat::getAll();
        $ents = ['prj', 'dep', 'reg'];
        foreach ($ents as $ent) {
            if (array_key_exists($ent, $changedAttributes) && $this->{$ent}) {

                foreach ($allFormats as $format) {
                    if ($format->{$ent} && $format->code == DetailFormat::CODE_DEFAULT) {
                        $categoryIds = $format->categories_ids;
                        if (!empty($categoryIds) && !in_array($this->id, $categoryIds)) {
                            $addToFormats[$format->id] = $format->id;
                        }
                    }
                }
            }
        }

        if (count($addToFormats)) {
            $batchInsertArr = [];
            foreach ($addToFormats as $formatId) {
                $batchInsertArr[] = [
                    'format_id' => $formatId,
                    'category_id' => $this->id
                ];
            }
            \Yii::$app->db->createCommand()->batchInsert('detail_formats_categories',
                ['format_id', 'category_id'], $batchInsertArr)->execute();
        }
    }

    public function beforeSave($insert)
    {
//        if ($this->is_project_manage) {
//            $this->is_internal = true;
//        }
        return parent::beforeSave($insert);
    }

    public function up()
    {
        $prev = static::find()
            ->andWhere(['<', 'sort', $this->sort])
//            ->andWhere(['delete_date' => null])
            ->orderBy(['sort' => SORT_DESC])
            ->limit(1)
            ->one();
        if ($prev) {
            $prevSort = $prev->sort;
            $prev->sort = $this->sort;
            $this->sort = $prevSort;
            $prev->save(false, ['sort']);
            $this->save(false, ['sort']);
        }
    }

    public function down()
    {
        $next = static::find()
            ->andWhere(['>', 'sort', $this->sort])
//            ->andWhere(['delete_date' => null])
            ->orderBy(['sort' => SORT_ASC])
            ->limit(1)
            ->one();
        if ($next) {
            $nextSort = $next->sort;
            $next->sort = $this->sort;
            $this->sort = $nextSort;
            $next->save(false, ['sort']);
            $this->save(false, ['sort']);
        }
    }

}