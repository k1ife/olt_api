<?php
namespace common\models\dict;

use common\models\data\Department;
use common\models\data\Project;
use common\models\data\Region;
use yii\base\Model;

/**
 * Class Role
 * @package common\models\dict
 *
 * @property string $label
 */
class Role extends Model
{
    public $code;
    public $name;
    public $masterClass;
    public $isAdminRole;
    public $isLeaderRole;
    public $hasSubroles;

    const ROLE_ADMIN = 100;           // admin(super user)
    const ROLE_USER = 1;

    // roles - not settings to user account - only to project or department users
//    const ROLE_PROFESSIONAL = 12;   //
    const ROLE_DEP_ADMIN = 80;           // federation(department) admin
//    const ROLE_DEP_LEADER = 75;     // federation(department) leader

    const ROLE_REG_ADMIN = 60;       // region admin
//    const ROLE_REG_LEADER = 55;     // region leader

    const ROLE_PROJECT_LEADER = 25;     // project leader
    const ROLE_PROJECT_PARTICIPANT = 20; // project participant
    const ROLE_PROJECT_GUEST = 18;      // project guest

    public function __construct($code, $label, $master_class,
        $is_admin = false, $is_leader = false, $hasSubroles = false)
    {
        $this->code = $code;
        $this->name = \Yii::t('app', $label) ;
        $this->masterClass = $master_class;
        $this->isAdminRole = $is_admin ?: false;
        $this->isLeaderRole = $is_leader ?: false;
        $this->hasSubroles = $hasSubroles ?: false;
    }


    static protected $_all = null;
    static public function getAll()
    {
        if (!static::$_all) {
            static::$_all = [
                self::ROLE_ADMIN =>
                    new Role(self::ROLE_ADMIN, 'super admin', null, true),
                self::ROLE_DEP_ADMIN =>
                    new Role(self::ROLE_DEP_ADMIN,  'dep admin', Department::class, true),
//                self::ROLE_DEP_LEADER =>
//                    new Role(self::ROLE_DEP_LEADER,  'dep leader', Department::class, false, true),

                self::ROLE_REG_ADMIN =>
                    new Role(self::ROLE_REG_ADMIN,  'reg admin', Region::class, true),
//                self::ROLE_REG_LEADER =>
//                    new Role(self::ROLE_REG_LEADER,  'reg leader', Region::class, false, true),

                self::ROLE_PROJECT_LEADER =>
                    new Role(self::ROLE_PROJECT_LEADER,  'project leader', Project::class, false, true),
                self::ROLE_PROJECT_PARTICIPANT =>
                    new Role(self::ROLE_PROJECT_PARTICIPANT,  'project participant',
                        Project::class, false, false, false),
//                        Project::class, false, false, true),
                self::ROLE_PROJECT_GUEST =>
                    new Role(self::ROLE_PROJECT_GUEST,  'project guest',
                        Project::class, false, false, false),
            ];
        }

        return static::$_all;
    }

    /**
     * @param $id
     * @return Role
     */
    public static function findById($id)
    {
        return self::getByCode($id);
    }

    public static function getByCode($code)
    {
        $all = self::getAll();
        if (isset($all[$code])) {
            return $all[$code];
        } else {
            return new Role($code, '?', null);
        }
    }

    public function getCaption()
    {
        return $this->name;
    }
}