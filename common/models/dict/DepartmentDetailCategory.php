<?php
namespace common\models\dict;

use common\models\data\Department;

/**
 * Class DepartmentDetailCategory
 * @package common\models\dict
 *
 * @property integer $id [integer]
 * @property string $name
 * @property string $color
 *
 *
 *
 */
class DepartmentDetailCategory extends DetailCategory
{
    public static function find()
    {
        return parent::find()
            ->andWhere([Department::getEntityCode() => true]);
    }

    public function beforeSave($insert)
    {
        $this->dep = true;
        return parent::beforeSave($insert);
    }


    static private $_all;
    static protected function getAllCached()
    {
        return static::$_all;
    }

    static protected function setAllCached($all)
    {
        static::$_all = $all;
    }


}