<?php
namespace common\models\dict;

use common\models\data\Project;

/**
 * Class ProjectDetailCategory
 * @package common\models\dict
 *
 * @property integer $id [integer]
 * @property string $name
 *
 */
class ProjectDetailCategory extends DetailCategory
{
    public static function find()
    {
        return parent::find()
            ->andWhere([Project::getEntityCode() => true]);
    }

    public function beforeSave($insert)
    {
        $this->prj = true;
        return parent::beforeSave($insert);
    }


    static private $_all;
    static protected function getAllCached()
    {
        return static::$_all;
    }

    static protected function setAllCached($all)
    {
        static::$_all = $all;
    }

    /**
     * @return ProjectDetailCategory|null
     */
    static public function findProjectManageCategory()
    {
        return static::find()->andWhere(['is_project_manage' => true])->one();
    }

}