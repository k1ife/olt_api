<?php


namespace common\models\dict;

/**
 * Class MeetingHelpText
 *
 * @property string $id [integer]
 * @property string $text
 *
 */
class MeetingHelpText extends BaseDict
{
    public static function tableName()
    {
        return "meeting_help_texts";
    }

    public function rules()
    {
        return [
            ["id", 'integer'],
            ['text', 'string'],

            ["text", "unique"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => \Yii::t("app", "Text")
        ];
    }

    protected static function getMainField() {
        return "text";
    }

    static private $_all;
    static protected function getAllCached()
    {
        return static::$_all;
    }

    static protected function setAllCached($all)
    {
        static::$_all = $all;
    }

}