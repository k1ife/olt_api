<?php

namespace common\models\dict;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

abstract class BaseDict extends ActiveRecord
{

    abstract static protected function getAllCached();
    abstract static protected function setAllCached($all);

    protected static function getMainField() {
        return "name";
    }

    protected static function getSortField() {
        return self::getMainField();
    }

    private static function fillAllInternal()
    {
        $sort_field = static::getSortField();
        $all = static::getAllCached();
        if (!$all){
            $query = static::find()->orderBy([$sort_field => SORT_ASC]);
            $all = $query->all();

//            // to use i18 translation
//            foreach ($all as &$item){
//                $item->name = \Yii::t('app', $item->name);
//            }

            static::setAllCached($all);
        }
        return $all;
    }

    /**
     * @return static[]
     */
    public static function getAll()
    {
        return static::fillAllInternal();
    }

    /**
     * @return array [id] => "name"
     */
    public static function getAllArr()
    {
        $all = static::getAll();
        $field = static::getMainField();
        $res = ArrayHelper::map($all, 'id', $field);
        return $res;
    }

    /**
     * @param $id
     * @return static|null
     */
    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }
}