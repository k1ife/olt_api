<?php

namespace common\models\dict;


/**
 * Class ProjectType
 * @property integer $id
 * @property string $name
 * @property string $short_name
 */
class ProjectType extends BaseDict
{
    public static function tableName()
    {
        return 'project_types';
    }

    public function rules()
    {
        return [
            ['id', 'integer'],
            ['name', 'string'],
            ['short_name', 'string'],

            ["name", "unique"],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => \Yii::t('app', 'Name'),
            'short_name' => \Yii::t('app', 'Short name'),
        ];
    }

    static private $_all;
    static protected function getAllCached()
    {
        return static::$_all;
    }

    static protected function setAllCached($all)
    {
        static::$_all = $all;
    }

}