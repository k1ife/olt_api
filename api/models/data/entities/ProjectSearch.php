<?php

namespace api\models\data\entities;

use common\models\data\Department;
use common\models\data\Project;
use common\models\data\Region;
use common\models\data\User;
use common\models\data\UserRole;
use common\models\data\UserRoles;
use common\models\dict\Role;
use common\models\utils\ITreeObject;
use common\models\utils\Tree;
use yii\helpers\ArrayHelper;

class ProjectSearch
{

    const PROJECTS_MINE_ALL = '';
    const PROJECTS_MINE_LEADER = 'leader';
    const PROJECTS_MINE_PARTICIPANT = 'partic';
    const PROJECTS_MINE_GUEST = 'guest';
    const PROJECTS_MINE_ARCHIVE = 'archive';

    const PROJECTS_ALL_ALL = 'all';
    const PROJECTS_ALL_ARCHIVE = 'all-archive';

    public static function getProjectsRequestTypes()
    {
        return [
            self::PROJECTS_MINE_ALL         => \Yii::t('app', 'All'),
            self::PROJECTS_MINE_LEADER      => \Yii::t('app', 'Leader'),
            self::PROJECTS_MINE_PARTICIPANT => \Yii::t('app', 'Participant'),
            self::PROJECTS_MINE_GUEST       => \Yii::t('app', 'Guest'),
            self::PROJECTS_MINE_ARCHIVE     => \Yii::t('app', 'Archive'),
        ];
    }

    public static function getAllProjectsRequestTypes()
    {
        return [
            self::PROJECTS_ALL_ALL     => \Yii::t('app', 'All'),
            self::PROJECTS_ALL_ARCHIVE => \Yii::t('app', 'Archive'),
        ];
    }

    public static function getQueryForUser(
        User $user = null,
        $opened = true,
        $as_role = null,
        $search = null,
        $department_id = null,
        $region_id = null
    ) {
        $query = Project::find();

//        $user = User::curr($user);
//        if (!$user){
//            return $query->andWhere(new Expression('0=1'));
//        }

        if ($opened === true) {
            $query->andWhere(['closed_at' => null]);
        } elseif ($opened === false) {
            $query->andWhere(['not', ['closed_at' => null]]);
        }

        if ($department_id) {
            $query->innerJoin('projects_departments', 'projects_departments.project_id = projects.id ')
                ->andWhere(['projects_departments.department_id' => $department_id]);
        }
        if ($region_id) {
            $query->innerJoin('projects_regions', 'projects_regions.project_id = projects.id ')
                ->andWhere(['projects_regions.region_id' => $region_id]);
        }

        if ($user) {
            if ($as_role) {

                switch ($as_role) {
                    case Role::ROLE_PROJECT_LEADER:
                        $query
                            ->innerJoin(['ur' => UserRole::tableName()], 'projects.id = ur.project_id')
                            ->andWhere(['ur.user_id' => $user->id, 'ur.role_code' => Role::ROLE_PROJECT_LEADER]);
                        break;

                    case Role::ROLE_PROJECT_PARTICIPANT:
                        $query
                            ->innerJoin(['ur' => UserRole::tableName()], 'projects.id = ur.project_id')
                            ->andWhere(['ur.user_id' => $user->id, 'ur.role_code' => Role::ROLE_PROJECT_PARTICIPANT]);
                        break;

                    case Role::ROLE_PROJECT_GUEST:
                        $query
                            ->innerJoin(['ur' => UserRole::tableName()], 'projects.id = ur.project_id')
                            ->andWhere(['ur.user_id' => $user->id, 'ur.role_code' => Role::ROLE_PROJECT_GUEST]);
                        break;

                    case Role::ROLE_ADMIN:
                        break;
                }

            } else {

                if ($user->isSuperAdmin()) {
//                     all projects are available for admin

                } else {
                    // for other users available only projects where they are prof of leaders
                    $query
                        ->innerJoin(['ur' => UserRole::tableName()], 'projects.id = ur.project_id')
                        ->andWhere(['ur.user_id' => $user->id ]);
                }
            }
        }

        if ($search) {
            $query->andWhere(['ilike', 'name', $search]);
        }

        $query->orderBy(['name' => SORT_ASC]);

        return $query;
    }

    /**
     * @param User $user
     * @param boolean $opened
     * @param integer $as_role
     * @param string $search
     * @param null $department_id
     * @param null $region_id
     * @return Project[]
     */
    public static function findForUser(
        User $user = null,
        $opened = true,
        $as_role = null,
        $search = null,
        $department_id = null,
        $region_id = null
    ) {
        $query = self::getQueryForUser($user, $opened, $as_role, $search, $department_id, $region_id);

        return $query->all();
    }

    /**
     * Generates projects tree for user depending on his access visibility
     *
     * @param ITreeObject[] $projects
     * @param null $department_id
     * @param null $region_id
     * @return Tree
     */
    public static function createProjectTree($projects, $department_id = null, $region_id = null) {

        $tree = static::prepareItemsForTree3($projects, $department_id, $region_id);
        $tree->sortRootItems(
            function ($a, $b) {
                if (is_a($a, ApiRegionShort::class) && is_a($b, ApiRegionShort::class)) {
                    /** @var ApiRegionShort $a */
                    /** @var ApiRegionShort $b */
                    if (($a->isCentral && $b->isCentral) || (!$a->isCentral && !$b->isCentral)) {
                        return strcmp($a->name, $b->name);
                    }
                    if ($a->isCentral) {
                        return -1;
                    }
                    if ($b->isCentral) {
                        return 1;
                    }
                } elseif (is_a($a, ApiProjectShort::class) || is_a($b, ApiProjectShort::class)) {
                    if (!is_a($a, ApiProjectShort::class)) {
                        return -1;
                    } elseif (!is_a($b, ApiProjectShort::class)) {
                        return 1;
                    }
                }
                return strcmp($a->name, $b->name);
            }
        );

        return $tree;
    }


    /**
     * @param ITreeObject[] $projects project data as Project
     * @param null $departmentId
     * @param null $region_id
     * @return Tree
     */
    private static function prepareItemsForTree3(array $projects, $departmentId = null, $region_id = null)
    {
        $tree = new Tree();

        $projectsApi = ApiProjectShort::generateArray($projects);
        $groupObjectsApi = [];
        $groupPref = null;
        // prepare groups (Deps or Regions)
        if ($departmentId) {
            // if filter by department - grouping by regions
            $regIds = [];
            foreach ($projectsApi as $projectApi) {
                $projRegIds = $projectApi->regionIds;
                if (is_array($projRegIds)) {
                    foreach ($projRegIds as $regId) {
                        $regIds[$regId] = null;
                    }
                }
            }
            $regions = Region::findByIds(array_keys($regIds));
            $groupObjectsApi = ArrayHelper::map($regions, 'id', function($reg) { return new ApiRegionShort($reg); });
            $groupPref = ApiBaseEntity::ENTITY_REG;
        } else {
            // grouping by departments
            $depIds = [];
            foreach ($projectsApi as $projectApi) {
                $projDepIds = $projectApi->departmentIds;
                if (is_array($projDepIds)) {
                    foreach ($projDepIds as $depId) {
                        $depIds[$depId] = null;
                    }
                }
            }
            $departments = Department::findByIds(array_keys($depIds));
            $groupObjectsApi = ArrayHelper::map($departments, 'id', function($dep){ return new ApiDepartmentShort($dep); });
            $groupPref = ApiBaseEntity::ENTITY_DEP;
        }

        $tree->makeTree($projectsApi);

        foreach ($tree->getRootItems() as $rootItem) {
            /** @var ApiProjectShort $rootProject */
            $rootProject = $rootItem->getData();
            $groupIds = $departmentId ? $rootProject->regionIds : $rootProject->departmentIds;

            if (!empty($groupIds)) {
                // if groups for project is empty - leave project at root level
                // but in other case - make it as child of group
                // also project can be as more than one group child

                $groupItems = [];
                foreach ($groupIds as $groupId) {
                    if (isset($groupObjectsApi[$groupId])) {
                        $groupObjectApi = $groupObjectsApi[$groupId];
                        $groupItemId = $groupPref.'_'.$groupId;
                        $groupItems[] = $tree->addRootItem($groupItemId, $groupObjectApi);
                    }
                }
                if (!empty($groupItems)) {
                    $tree->moveItemToAnotherBranches($rootItem, $groupItems);
                }
            }
        }
        return $tree;
    }

    /**
     * Counts projects where user is leader
     * @param $user_id
     * @return int
     */
    public static function countProjectsAsLeaderIn($user_id)
    {
        $query = Project::find()
            ->select(['id'])
            ->where(['closed_at' => null])
            ->andWhere(['leader_id' => $user_id])
        ;
        return $query->count();
    }

    /**
     * Counts projects where user is participants
     *
     * @param $user_id
     * @return int|string
     */
    public static function countProjectsAsParticipantIn($user_id)
    {
        $query = Project::find()
            ->innerJoin('projects_participants', Project::tableName().'.id = project_id')
            ->select([Project::tableName().'.id'])
            ->where(['closed_at' => null])
            ->andWhere(['user_id' => $user_id])
        ;
        return $query->count();
    }

    /**
     * @param $search_project_term
     * @param $projects_selector
     * @param null $department_id
     * @param null $region_id
     * @return Project[]
     */
    public static function findProjects($search_project_term,
        $projects_selector, $department_id = null, $region_id = null)
    {
        $projects = null;
        switch ($projects_selector){

            // all projects
            case self::PROJECTS_ALL_ALL:
                $projects = self::findForUser(
                    null, true, null, $search_project_term, $department_id, $region_id);
                break;

            case self::PROJECTS_ALL_ARCHIVE:
                $projects = self::findForUser(
                    null, false, null, $search_project_term, $department_id, $region_id);
                break;

            // mine projects
            case self::PROJECTS_MINE_ARCHIVE:
                $projects = self::findForUser(
                    User::curr(), false, null, $search_project_term, $department_id, $region_id);
                break;
            case self::PROJECTS_MINE_LEADER:
                $projects = self::findForUser(
                    User::curr(), true, Role::ROLE_PROJECT_LEADER, $search_project_term, $department_id, $region_id);
                break;
            case self::PROJECTS_MINE_PARTICIPANT:
                $projects = self::findForUser(
                    User::curr(), true, Role::ROLE_PROJECT_PARTICIPANT, $search_project_term, $department_id, $region_id);
                break;
            case self::PROJECTS_MINE_GUEST:
                $projects = self::findForUser(
                    User::curr(), true, Role::ROLE_PROJECT_GUEST, $search_project_term, $department_id, $region_id);
                break;

            default:
                // mine all
                // for dep admin (task #868):
                // Department admin should see all projects of the department where he/she is an admin in the "Alle - mine" filter.
                $user = User::curr();
                if ($department_id) {
                    $dep = Department::findById($department_id);
                    if ($dep && UserRoles::hasRole($user, Role::ROLE_DEP_ADMIN, $dep)) {
                        // user is dep admin
                        $projects = self::findForUser(
                            $user, true, Role::ROLE_DEP_ADMIN, $search_project_term, $department_id, $region_id);
                    }
                }
                if (!$projects) {
                    $projects = self::findForUser(
                        $user, true, null, $search_project_term, $department_id, $region_id);
                }
        }
        return $projects;
    }
}