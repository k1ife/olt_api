<?php
namespace api\models\data\entities;

use api\models\data\users\ApiUserShort;
use common\models\data\base\BaseMasterEntity;
use common\models\data\Project;
use common\models\utils\ITreeObject;

class ApiProjectShort extends ApiBaseEntityShort implements ITreeObject
{
//    public $id;
//    public $name;
//    public $entityCode;

//    public $role;
//    public $canView;
//    public $canEdit;

    public $parentId;
    public $leaders;
    public $description;
    public $regionIds;
    public $departmentIds;

    /**
     * @return class-string<BaseMasterEntity>
     */
    public static function baseModelClass()
    {
        return Project::class;
    }

    /**
     * @param Project $model
     * @param null $extData
     */
    protected function fillByModel($model, $extData = null)
    {
        parent::fillByModel($model, $extData);

        $this->leaders = ApiUserShort::generateArray($model->leaders);
        $this->regionIds = $model->getRegionsIds();
        $this->departmentIds = $model->departments_ids;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getParentId()
    {
        return $this->parentId;
    }


}