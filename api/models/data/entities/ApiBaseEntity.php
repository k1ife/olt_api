<?php
namespace api\models\data\entities;

use api\models\base\BaseApiModel;
use common\models\data\base\BaseMasterEntity;

abstract class ApiBaseEntity extends BaseApiModel
{
    public $id;
    public $name;
    public $entityCode;

    public const ENTITY_PROJ    = BaseMasterEntity::ENTITY_PROJ;
    public const ENTITY_DEP     = BaseMasterEntity::ENTITY_DEP;
    public const ENTITY_REG     = BaseMasterEntity::ENTITY_REG;

    /**
     * @return class-string<BaseMasterEntity>
     */
    abstract public static function baseModelClass();

    /**
     * @param BaseMasterEntity $model
     * @param null $extData
     */
    protected function fillByModel($model, $extData = null)
    {
        parent::fillByModel($model, $extData);
        $this->entityCode = $model->getMyEntityCode();
    }


}