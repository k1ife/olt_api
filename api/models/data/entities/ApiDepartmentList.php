<?php
namespace api\models\data\entities;

use api\models\base\BaseApiModel;
use common\models\data\Department;

class ApiDepartmentList extends BaseApiModel
{
    public $id;
    public $name;
    public $image;

    public static function baseModelClass()
    {
        return Department::class;
    }


}