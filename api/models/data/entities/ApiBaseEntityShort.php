<?php
namespace api\models\data\entities;


use common\models\data\base\BaseMasterEntity;
use common\models\data\User;

abstract class ApiBaseEntityShort extends ApiBaseEntity
{
    public $role;
    public $canEdit;
    public $canView;

    /**
     * @param BaseMasterEntity $model
     * @param null $extData
     */
    protected function fillByModel($model, $extData = null)
    {
        parent::fillByModel($model, $extData);

        $user = User::curr();

        $this->role = $model->getMaxUserRoleCode($user);
        $this->canView = $model->canView($user);
        $this->canEdit = $model->canEdit($user);
    }


}