<?php
namespace api\models\data\entities;

use api\models\data\users\ApiUserShort;
use common\models\data\Department;

class ApiDepartmentShort extends ApiBaseEntityShort
{
//    public $id;
//    public $name;
//    public $entityCode;

//    public $role;
//    public $canView;
//    public $canEdit;

    public $admins;


    public static function baseModelClass()
    {
        return Department::class;
    }

    /**
     * @param Department $model
     * @param null $extData
     */
    protected function fillByModel($model, $extData = null)
    {
        parent::fillByModel($model, $extData);

        $this->admins = ApiUserShort::generateArray($model->admins);
    }


}