<?php

namespace api\models\data\users;

use api\models\base\BaseApiModel;
use common\models\data\User;

class ApiUserProfile extends BaseApiModel
{
    public $id;
    public $email;
    public $firstName;
    public $lastName;
    public $status;
    public $phone;
    public $profession;
    public $photo;
    public $sendProjectNotifications;

    public $isSA; // is super admin
    public $roles;

    public static function baseModelClass()
    {
        return User::class;
    }

    /**
     * @param User $model
     * @param null $extData
     */
    protected function fillByModel($model, $extData = null)
    {
        parent::fillByModel($model, $extData);

        $this->isSA = $model->isSuperAdmin();
        $this->roles = $model->getUserRoles();
    }


}