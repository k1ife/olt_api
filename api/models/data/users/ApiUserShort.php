<?php
namespace api\models\data\users;

use api\models\base\BaseApiModel;
use common\models\data\User;

class ApiUserShort extends BaseApiModel
{
    public $id;
    public $name;

    public static function baseModelClass()
    {
        return User::class;
    }

    /**
     * @param User $model
     * @param null $extData
     */
    protected function fillByModel($model, $extData = null)
    {
        parent::fillByModel($model, $extData); // TODO: Change the autogenerated stub
        $this->name = trim(implode(' ', [$model->first_name, $model->last_name]));
        if (!$this->name) {
            $this->name = $model->email;
        }
    }


}