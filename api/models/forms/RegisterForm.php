<?php
namespace common\forms;

use common\models\companies\Company;
use common\models\users\User;
use common\models\users\UserContact;
use Yii;
use yii\base\Model;

/**
 * Signup form
 */
class RegisterForm extends Model
{
    public $email;
    public $password;
    public $typeCode;

    private $generatedPassword = null;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
//            ['email', 'unique', 'targetClass' => '\common\models\users\User', 'message' => 'This email address has already been taken.'],

//            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['typeCode', 'integer'],
            ['typeCode', 'required']
        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);
        $userContact = UserContact::findUserByContact(UserContact::CODE_EMAIL, $this->email);
        if ($userContact) {
            $this->addError('email', 'This email address has already been taken.');
        }

        return !$this->hasErrors();
    }


    public static function generatePassword()
    {
        $password = Yii::$app->security->generateRandomString(8);
        return $password;
    }

    public function getGeneratedPassword()
    {
        return $this->generatedPassword;
    }

    /**
     * Signs user up.
     *
     * @return User|null
     */
    public function register()
    {
        if (!$this->validate()) {
            return null;
        }

        if ($this->password) {
            $password = $this->password;
        } else {
            $password = static::generatePassword();
            $this->generatedPassword = $password;
        }

        $passwordHash = Yii::$app->security->generatePasswordHash($password);
//        $token = Yii::$app->security->generateRandomString() . '_' . time();
        $params = [
            'password' => $passwordHash,
        ];
        $user = User::createUserBySoc($this->typeCode, UserContact::CODE_EMAIL, $this->email, $params);
        if ($user && $this->sendEmail($user, $this->email, $this->generatedPassword)) {
            return $user;
        } else {
            return null;
        }
    }

    public function getAuthData()
    {
        return [
            'email' => $this->email,
            'password' => $this->password ?: $this->generatedPassword
        ];
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @param string $email
     * @param string $generatedPassword - пароль, сформированный сервером (не хешированный)
     * @return bool whether the email was sent
     */
    protected function sendEmail($user, $email, $generatedPassword)
    {


//        $verifyLink = Yii::$app->urlManager->createAbsoluteUrl(
//            ['auth/verify-email', 'token' => $token]);

        return Yii::$app
            ->mailer
            ->compose(
//                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['html' => 'newUser-html', 'text' => 'newUser-text'],
                [
                    'user' => $user,
                    'password' => $generatedPassword,
//                    'verifyLink' => $verifyLink,
                ]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($email)
            ->setSubject('Account registration at ' . Yii::$app->name)
            ->send();
    }
}
