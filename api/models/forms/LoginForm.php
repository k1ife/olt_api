<?php
namespace api\models\forms;

use common\models\data\User;
use yii\base\Model;

/**
 * Signup form
 */
class LoginForm extends Model
{
    public $email;
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['password', 'required'],
        ];
    }

    public function login()
    {
        return User::findByEmailAndPassword($this->email, $this->password);
    }


}
