<?php

namespace api\models\forms\profile;

use api\models\base\BaseApiForm;
use common\models\data\User;

class ProfileForm extends BaseApiForm
{
    public $firstName;
    public $lastName;
    public $email;
    public $phone;
    public $profession;
    public $sendProjectNotifications;

    public function rules()
    {
        return [
            [['firstName', 'lastName'], 'string'],
            ['email', 'email'],
            ['phone', 'string'],
            ['profession', 'string'],
            ['sendProjectNotifications', 'boolean']
        ];
    }

    public function updateProfile(User $user)
    {
        $user->first_name = $this->firstName;
        $user->last_name = $this->lastName;
        $user->email = $this->email;
        $user->phone = $this->phone;
        $user->profession = $this->profession;
        $user->send_project_notifications = $this->sendProjectNotifications;
        if (!$user->save()) {
            $this->addErrors($user->errors);
            return false;
        }
        return true;
    }


}