<?php

namespace api\models\forms\profile;

use api\models\base\BaseApiForm;
use common\models\data\User;

class SetPasswordForm extends BaseApiForm
{

    public $password1;
    public $password2;

    public function rules()
    {
        return [
            ['password1', 'string'],
            ['password2', 'string'],

        ];
    }

    public function validate($attributeNames = null, $clearErrors = true)
    {
        parent::validate($attributeNames, $clearErrors);

        if (!$this->password1) {
            $this->addError('password1', 'EnterPassword');
        }
        if ($this->password2 !== $this->password1) {
            $this->addError('password2', 'PasswordsNotEqual');
        }

        return !$this->hasErrors();
    }

    public function setPassword(User $user)
    {
        if ($this->validate()) {

            $user->setPassword($this->password1);
            $user->save(false);
            return true;
        }
        return false;
    }


}