<?php

namespace api\models\base;

use yii\base\Model;

class BaseApiForm extends Model
{
    public function load($data, $formName = null)
    {
        return parent::load($data, $formName ?? '');
    }

}