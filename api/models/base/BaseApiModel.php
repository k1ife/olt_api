<?php
namespace api\models\base;

use yii\base\Model;
use yii\helpers\Inflector;

abstract class BaseApiModel extends Model
{

    abstract public static function baseModelClass();

    function __construct($baseModel, $extData = null)
    {
        parent::__construct();

        if (!empty($baseModel) ) {
            $baseModelClass = static::baseModelClass();

//            $msg = "Config is obj of class: ".get_class($config).', baseModelClass: '. $baseModelClass;

            if (is_a($baseModel, $baseModelClass)) {
                $this->fillByModel($baseModel, $extData);
            }
        }
    }


    /**
     * @param array $models
     */
    protected static function fillExtDataCache($models)
    {
        // доп функция, которая вызывается перед заполнением,
        // для запроса кеша доп данных
    }

    public static function generateArray($models, $extData = null)
    {
        if (!$models){
            return [];
        }
        static::fillExtDataCache($models);

        $res = [];
        foreach ($models as $model){
            $new = new static($model, $extData);
            $res[] = $new;
        }
        return $res;
    }


    protected function thumbUrlToAbsolute($relative_url)
    {
        return \Yii::$app->params['protocol'].'://'.\Yii::$app->params['domain'].$relative_url;
    }

    protected function populateByBaseModel(Model $baseModel)
    {
        $attrs = $baseModel->attributes();
        foreach ($attrs as $name) {
            $camelName = Inflector::variablize($name);
//            $names[] = "$name => $camelName : $value";

            if ($this->hasProperty($camelName)) {
                $value = $baseModel->$name;
                $this->{$camelName} = $value;
            }
        }
//        \Yii::warning("__________\n".join("\n", $names));
    }

    protected function fillByModel($model, $extData = null)
    {
        static::fillExtDataCache([$model]);
        $this->populateByBaseModel($model);
//
//        $baseClass = static::baseModelClass();
//        if ($baseClass) {
//
//        } else {
//            $className = static::class;
//            throw new \Exception("Implement baseModelClass() OR override fillByModel() for class $className");
//        }
    }
} 