<?php
namespace api\controllers;


use yii\web\Controller;

class TestController extends Controller
{

    public function actionMe()
    {
        return ["res" => "Hello, bro!"];
    }
}