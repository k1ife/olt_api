<?php
namespace api\controllers;

use api\controllers\base\BaseController;
use common\models\auth\JWT;
use api\models\forms\LoginForm;
use common\forms\RegisterForm;
//use GuzzleHttp\Client;

/**
 * Class AuthController
 * @package common\controllers
 *
 * контроллер реализует логин и регистрацию
 */
class AuthController extends BaseController
{

//    /**
//     * Вызывается при входе через google
//     * передает:
//     *  code - всегда разный - соответствует коду сессии в гугле
//     *  clientId постоянный - соответствует коду приложения
//     * здесь мы пытаемся узнать id пользователя в google
//     * и далее заводим у себя нового либо если уже имеется - возвращаем имющегося в нашей системе
//     * возвращаем JWT token
//     * @return mixed
//     */
//    public function actionGoogle()
//    {
//        $soc_code = 'goog';
//        $request = \Yii::$app->request;
//        $http = new Client();
//
//        // Step 1. Обменяем code на access_token
//        $accessTokenResponse = $http->request('POST',
//            'https://oauth2.googleapis.com/token', [
////            'https://accounts.google.com/o/oauth2/token', [
//            'form_params' => [
//                'code' => $request->getBodyParam('code'),
//                'client_id' => $request->getBodyParam('clientId'),
//                'client_secret' => \Yii::$app->params['social']['google']['clientSecret'],
//                'redirect_uri' => $request->getBodyParam('redirectUri'),
//                'grant_type' => 'authorization_code',
//            ]
//        ]);
//        $accessTokenArr = json_decode($accessTokenResponse->getBody(), true);
//        $accessToken = $accessTokenArr['access_token'];
//
//        // Step 2. Запросим профиль пользователя
//        $profileResponse = $http->request('GET',
//            'https://openidconnect.googleapis.com/v1/userinfo', [
//            'headers' => array('Authorization' => "Bearer $accessToken")
//        ]);
//
//        $profile = json_decode($profileResponse->getBody(), true);
////        $this->log("PROF: ". print_r($profile, true));
//        $uid = $profile['sub'];
//        $user = User::findBySoc($soc_code, $uid);
//
//        // Проверим есть ли в заголовках информация о том что пользователь уже залогинен
//        $authData = $request->headers->get('Authorization');
//        if ($authData)
//        {
//            // Пользователь залогинен
//            if ($user) {
//                // пользователь уже существует, аккаунт google  привязан
//                // ситуация исключительная - прерываемся
//                return $this->returnErrorMsg("There is already a $soc_code account that belongs to you", 409);
//
//            } else {
//                // пользователь залогинен, но google аккаут не привязан
//                $token = Utils::getAuthTokenFromHeaders();
//
//                $jwt = new JWT($token);
//                $user = User::findById($jwt->userId);
//                if (!$user) {
//                    throw new Exception("Not found auth user");
//                }
//                $this->updateUser($user, $soc_code, $uid, $profile);
//                return $this->responseAuth($user);
//            }
//        } else {
//            // пользователь не залогинен
//            if (!$user) {
//                $user = User::createUserBySoc($soc_code, $uid, $profile);
//            }
//            return $this->responseAuth($user);
//
//        }
//    }
//
//    /**
//     * @param User $user
//     * @param $soc_code
//     * @param $uid
//     * @param array $params
//     * @throws \Exception
//     */
//    protected function updateUser(User $user, $soc_code, $uid, $params)
//    {
//        $user->setSocUid($soc_code, $uid, json_encode($params, JSON_UNESCAPED_UNICODE));
//        $user->display_name = $user->display_name ?: $params['name'];
//        if (!$user->save()) {
//            throw new Exception("Error save User: " . Utils::getModelErrorsString($user));
//        }
//    }

    /**
     * Create Email and Password Account.
     */
    public function actionRegister()
    {
        $request = \Yii::$app->request;
        $params = $request->getBodyParams();
        $regForm = new RegisterForm();
        if ($regForm->load($params, '')) {
            if ($regForm->validate()) {

                $user = $regForm->register();
                if ($user) {
                    return $this->responseAuth($user);
                } else {
                    return $this->returnModelErrors($regForm, 400);
                }

            } else {
                return $this->returnModelErrors($regForm, 400);
            }
        } else {
            return $this->returnErrorMsg("Empty request", 400);
        }
    }

    /**
     * Log in with Email and Password.
     */
    public function actionLogin()
    {
        $request = \Yii::$app->request;
        $params = $request->getBodyParams();
        $loginForm = new LoginForm();
        if ($loginForm->load($params, '')) {
            if ($loginForm->validate()) {

                $user = $loginForm->login();
                if ($user) {
                    return $this->responseAuth($user);
                } else {
                    return $this->returnErrorMsg('Wrong email and/or password', 400);
                }

            } else {
                return $this->returnModelErrors($loginForm, 400);
            }
        } else {
            return $this->returnErrorMsg('Wrong email and/or password', 400);
        }
    }

//
//    public function actionConfirmEmail($token)
//    {
//        $contact = UserContact::findByConfirmToken($token, UserContact::CODE_EMAIL);
//        if ($contact) {
//            $contact->confirmContact();
//        }
//    }

    // Отправка ответа в формате клиентской библиотеки авторизации vue-authenticate

    protected function responseAuth($user)
    {
        $jwt = JWT::createToken($user);
        $this->log("Our JWT: $jwt");
        return ['access_token' => $jwt];
    }


}