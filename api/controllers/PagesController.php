<?php

namespace api\controllers;

use api\controllers\base\BaseController;
use common\models\data\Page;
use common\models\data\User;
use yii\web\NotAcceptableHttpException;
use yii\web\NotFoundHttpException;

class PagesController extends BaseController
{

    public function actionGetList()
    {
        $isGuest = User::isGuest();
        $pages = Page::getPagesForMenu($isGuest);
        return $this->returnItems($pages);
    }

    public function actionGetItem($id)
    {
        $slug = $id;
        $page = Page::findBySlug($slug);
        if (!$page) {
            throw new NotFoundHttpException();
        }
        if (!$page->guest_access && User::isGuest()) {
            throw new NotAcceptableHttpException();
        }

        return $this->returnOk($page);
    }
}