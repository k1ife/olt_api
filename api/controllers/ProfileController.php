<?php
namespace api\controllers;

use api\controllers\base\BaseAuthController;
use api\models\data\users\ApiUserProfile;
use api\models\forms\profile\ProfileForm;
use api\models\forms\profile\SetPasswordForm;

class ProfileController extends BaseAuthController
{
    public function actionProfile()
    {
        $user = $this->getUser();
        return new ApiUserProfile($user);
    }

    public function actionSetPassword()
    {
        $user = $this->getUser();
        $form = new SetPasswordForm();
        if ($form->load($this->getBodyParams())) {
            if ($form->setPassword($user)) {
                return $this->returnOk();
            }
            return $this->returnModelErrors($form);
        }
        return $this->returnEmptyDataRq();
    }

    public function actionUpdate()
    {
        $user = $this->getUser();
        $form = new ProfileForm();
        if ($form->load($this->getBodyParams())) {
            if ($form->updateProfile($user)) {
                return new ApiUserProfile($user);
            }
            return $this->returnModelErrors($form);
        }
        return $this->returnEmptyDataRq();
    }
}