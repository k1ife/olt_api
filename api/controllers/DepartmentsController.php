<?php

namespace api\controllers;

use api\controllers\base\BaseAuthController;
use common\models\data\Department;
use api\models\data\entities\ProjectSearch;
use common\models\data\User;

class DepartmentsController extends BaseAuthController
{
    public function actionGetItem($id)
    {
        $department = $this->getDepartment($id);

        if (!$department->canView(User::getCurrent())) {
            $this->accessDenied();
        }

        $projects = ProjectSearch::findProjects(null, null, $department->id);

        $tree = ProjectSearch::createProjectTree($projects, $department->id);
        return $this->returnOk([
            'department' => $department,
            'projectsTree' => $tree->getRootItems()
        ]);

    }


    /**
     * @param $id
     * @return null|Department
     */
    protected function getDepartment($id)
    {
        $department = Department::findById($id);
        if (!$department){
            $this->notFound();
        }
        return $department;
    }

}