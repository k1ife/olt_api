<?php
namespace api\controllers;

use api\controllers\base\BaseAuthController;
use api\models\data\entities\ApiDepartmentList;
use api\models\data\entities\ApiRegionList;
use common\models\data\Department;
use common\models\data\Region;
use common\models\data\User;
use yii\helpers\Url;

class IndexController extends BaseAuthController
{

    public function actionGetPageData()
    {
        Url::remember();

        $user = User::curr();
        $departments = Department::getAllForUser($user);
        $regions = Region::getAllForUser($user);

        return $this->returnOk([
            'departments' => ApiDepartmentList::generateArray($departments),
            'regions' => ApiRegionList::generateArray($regions),
        ]);
    }


}