<?php
namespace api\controllers\base;

use common\models\auth\JWT;
use common\models\data\User;
use common\models\utils\Utils;
use yii\base\Model;
use yii\data\DataProviderInterface;
use yii\data\Pagination;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class BaseController extends Controller
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::class,
                'cors' => [
                    'Origin' => \Yii::$app->params['corsOrigin'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                ]
            ],
        ];
    }


    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        $this->logApiRequest($action);
        $this->setLocal();


        $token = Utils::getAuthTokenFromHeaders();
        $jwt = new JWT($token);
        $user = User::findById($jwt->userId);
        if ($user && !$user->isDeleted()) {
            \Yii::$app->user->setIdentity($user);
        }

        return parent::beforeAction($action);
    }

    /**
     * Пробует установить значения языка и страны, полученные из заголовков
     */
    protected function setLocal()
    {
        $lang = $this->getLang();

    }


    protected function log($str)
    {
//        $file_dir = \Yii::$app->params['apiLogDir'];
//        $time = new \DateTime();
//        $time_str = $time->format('Y-m-d H:i:s');
//        $file_name = 'api_'.$time->format('Y-m-d').'.log';
//        $file_path = $file_dir.DIRECTORY_SEPARATOR.$file_name;
//        file_put_contents($file_path, $time_str.' '.$str."\n", FILE_APPEND);

        \Yii::info($str, 'api');
    }

    /**
     * @param $action
     * @throws \yii\base\InvalidConfigException
     */
    protected function logApiRequest($action)
    {
//        $lang = $user ? $user->profile->getLanguage() : "";
//        $lang = Localisation::getLang();
//        $imperson = User::isImpersonalised() ? '(IMPERSON)' : '';

        $user = User::getCurrent();
        $str = 'REQUEST: "'.\Yii::$app->request->method.'" '.Url::current()
            .", USER: ".($user ? '['.$user->id.'] '.$user->display_name.'  ': '-')
//            .", LANG: ".$lang
            .", ACTION: ".$action->actionMethod
            ."\n";
        $params_str = $str.print_r($this->getBodyParams(), true);
        $params_str = str_replace("\n", "\n                    ", $params_str);
        $this->log($params_str);

    }

    protected function getBodyParams()
    {
        return \Yii::$app->request->getBodyParams();
    }

    protected function getQueryParams()
    {
        $params = \Yii::$app->request->getQueryParams();
        foreach ($params as $param => &$val) {
            if ($val === 'null' || $val === 'undefined') {
                $val = null;
            }
        }
        return $params;

    }

    protected function getJsonAsObj()
    {
        $rqRaw = $this->request->getRawBody();
        return Json::decode($rqRaw, false);
    }

    protected function getJsonAsArr()
    {
        $rqRaw = $this->request->getRawBody();
        return Json::decode($rqRaw, true);
    }

    /**
     * получает язык из параметров или заголовков. Параметр - в приоритете
     */
    protected function getLang($onlyFromHeaders = false, $default = null)
    {
        $lang = $onlyFromHeaders ? null : \Yii::$app->request->getQueryParam('lang');
        if ($lang === 'undefined') {
            $lang = null;
        }

        if (!$lang) {
            $lang = \Yii::$app->request->headers->get('Lang');
            if ($lang == 'undefined') {
                $lang = null;
            }
        }

        if ($lang) {
            $arr = explode('-', $lang);
            return $arr[0];
        } else {
            return $default;
        }
    }



//    protected function getCompanyIdFromHeaders()
//    {
//        $companyId = \Yii::$app->request->headers->get('Company');
//        if (!$companyId || $companyId === 'null') {
//            return null;
//        }
//        return $companyId;
//    }

    protected function returnModel(Model $model)
    {
        if ($model->hasErrors()) {
            return $this->returnModelErrors($model);
        } else {
            return $this->returnOk($model);
        }
    }

    protected function returnModelErrors(Model $model, $httpCode = null)
    {
        if ($httpCode) {
            \Yii::$app->response->statusCode = $httpCode;
        }
        return [
            'res' => 'errors',
            'errors' => $model->getErrorSummary(false),
            'errorsFull' => $model->errors
        ];
    }

    protected function returnErrorMsg($messages, $httpCode = null)
    {
        if (!is_array($messages)) {
            $messages = [$messages];
        }
        if ($httpCode) {
            \Yii::$app->response->statusCode = $httpCode;
        }
        return [
            'res' => 'errors',
            'errors' => $messages,
            'errorsFull' => $messages
        ];
    }

    protected function returnEmptyDataRq()
    {
        throw new BadRequestHttpException('Empty data');
    }

    protected function returnNotFound($message = null)
    {
        throw new NotFoundHttpException($message);
    }

    public function acceessDenied()
    {
        throw new ForbiddenHttpException();
    }

    protected function returnOk($data = null)
    {
        $res = [
            'res' => 'ok'
        ];
        if ($data) {
            $res['data'] = $data;
        }
        return $res;
    }

    protected function returnItems($items, Pagination $pagination = null, $additionalInfo = [])
    {
        if (!$items) {
            $items = [];
        }
        $data = $additionalInfo;
        $data['items'] = $items;
        $data['count'] = count($items);

        if ($pagination) {
            $data['rowsNumber'] = $pagination->totalCount;
            $data['page'] = $pagination->page + 1;
            $data['pageSize'] = $pagination->pageSize;
            $data['pageCount'] = $pagination->pageCount;
        }
        return $this->returnOk($data);
    }

    protected function returnDP(DataProviderInterface $dp, $modelsClass = null, $additionalInfo = [])
    {
        $models = $dp->getModels();
        if ($modelsClass) {
            $models = $modelsClass::generateArray($models);
        }
        return $this->returnItems($models, $dp->getPagination(), $additionalInfo);
    }


    /**
     * @return User
     */
    protected function getUser()
    {
        return User::getCurrent();
    }

    protected function getUserId()
    {
        return User::getCurrentId();
    }
}