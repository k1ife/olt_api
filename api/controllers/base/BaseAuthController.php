<?php
namespace api\controllers\base;


use common\models\data\User;
use yii\web\ForbiddenHttpException;

class BaseAuthController extends BaseController
{

//    public function behaviors()
//    {
//        $behaviors = parent::behaviors();
//        $behaviors['authenticator'] = [
//            'class' => HttpBearerAuth::class,
//        ];
//        return $behaviors;
//    }


    public function beforeAction($action)
    {
        $res = parent::beforeAction($action);
        if ($res) {
            $this->getUser();
        }
        return $res;
    }

    /**
     * @return User
     * @throws ForbiddenHttpException
     */
    protected function getUser()
    {
        $user = User::getCurrent();
        if (!$user) {
            return $this->acceessDenied();
        }
        return $user;
    }

    protected function getUserId()
    {
        $userId = User::getCurrentId();
        if (!$userId) {
            return $this->acceessDenied();
        }
        return $userId;
    }

}