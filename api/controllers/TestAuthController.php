<?php
namespace api\controllers;


use api\controllers\base\BaseAuthController;

class TestAuthController extends BaseAuthController
{

    public function actionMe()
    {
        $user = $this->getUser();
        return ["res" => "Hello, bro: ".$user->id];
    }
}