<?php

return [

    'OPTIONS <path:.+>' => 'options/response-options',

    'GET test' => 'test/me',
    'GET test-auth' => 'test-auth/me',
    'POST auth/<action>' => 'auth/<action>',
    'GET profile' => 'profile/profile',
    'POST profile' => 'profile/update',
    'POST profile/password' => 'profile/set-password',

    'GET index' => 'index/get-page-data',

    /// common cases
    'GET <controller>/<id>' => '<controller>/get-item',
    'GET <controller>' => '<controller>/get-list',
    'POST <controller>/<id>' => '<controller>/update',
    'POST <controller>' => '<controller>/add',
    'DELETE <controller>/<id>' => '<controller>/delete'
];