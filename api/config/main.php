<?php

use yii\web\JsonResponseFormatter;
use yii\web\JsonParser;
use yii\web\MultipartFormDataParser;
use yii\log\FileTarget;
use common\models\data\User;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
$api_url_rules = require(__DIR__ . '/url_rules.php');

return [
    'id' => 'olt-api',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'api\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'baseUrl' => '/api',
            'enableCsrfValidation' => false,
            'enableCookieValidation' => false,
            'parsers' => [
                'multipart/form-data' => MultipartFormDataParser::class,
                'application/json' => JsonParser::class
            ],

        ],
        'response' => [
            'format' => yii\web\Response::FORMAT_JSON,
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => JsonResponseFormatter::class,
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
//                    'encodeOptions' => JSON_UNESCAPED_SLASHES, // to trans unicode to format \uxxxx
                ],
            ],
            'on beforeSend' => function ($event) {
                // remove all data when 500 error
                // https://github.com/yiisoft/yii2/issues/14395

                /** @var \yii\web\Response $response */
                $response = $event->sender;
                if ($response->statusCode >= 500 && $response->statusCode < 600) {
                    // remove all, to hide debug error data from client
                    $response->data = [
                        'res' => 'errors',
                        'errors' => 'ServerError'
                    ];
                }
            },
        ],
        'user' => [
            'identityClass' => User::class,
            'enableAutoLogin' => false,
            'identityCookie' => ['name' => '_identity-api', 'httpOnly' => true],
            'enableSession' => false,
            'loginUrl' => null,
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'olt-api',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'levels' => ['error', 'warning'],
                    'logVars' => []
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => $api_url_rules,
            // use hostInfo - front address - to generating links through front, not api
            'hostInfo' => $params['protocol'].'://'.$params['frontDomain'],
        ],
    ],
    'aliases' => [
        '@images' => '@app/web/images',
        '@filesDir' => '@app/web/files',
    ],
    'params' => $params,
];
