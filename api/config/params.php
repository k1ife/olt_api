<?php
return [
    'apiLogDir' => "logs-api",
    'corsOrigin' => ['*'],

    'protocol' => 'http',
    'frontDomain' => 'olt2.my'
];
